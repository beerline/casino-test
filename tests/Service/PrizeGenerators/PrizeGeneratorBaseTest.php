<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 01.12.2018
 * Time: 20:53
 */

namespace App\Tests\Service\PrizeGenerators;

use App\Entity\User;
use App\Service\Banking\BankOfAmericaBanking;
use App\Tests\AbstractCasinoServicesTest;

abstract class PrizeGeneratorBaseTest extends AbstractCasinoServicesTest
{
    protected function setUser( $class, $filedInTableContainUser )
    {
        $user = new User(
            'test_user@example.com',
            BankOfAmericaBanking::BANK_ALIAS
        );
        $user->setPassword('123');


        $this->objectManager->persist($user);
        $this->objectManager->flush();

        $this->mocServiceProperty( $class, $filedInTableContainUser, $user );
    }
}