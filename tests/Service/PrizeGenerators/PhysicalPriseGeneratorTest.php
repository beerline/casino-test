<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 02.12.2018
 * Time: 16:38
 */

namespace App\Tests\Service\PrizeGenerators;

use App\Entity\PhysicalPrizeAvailable;
use App\Entity\PhysicalPrizeTransaction;
use App\Entity\User;
use App\Service\Banking\BankOfAmericaBanking;
use App\Service\PrizeGenerators\PhysicalPrize;
use App\Service\PrizeGenerators\Randomizer\PhysicalPrize\PhysicalPrizeSimpleRandomizer;
use App\Tests\AbstractCasinoServicesTest;
use App\GameEntity\PhysicalPrize as Prize;

class PhysicalPriseGeneratorTest extends AbstractCasinoServicesTest
{
    const COUNT_AVAILABLE_IPHONES = 100;

    const COUNT_AVAILABLE_IPAD = 10;

    const COUNT_AVAILABLE_MACBOOK = 50;

    private function loadAvailablePhysicalPrize(  )
    {
        $iphonesAvailable = new PhysicalPrizeAvailable(
            true,
            'iPhone',
            static::COUNT_AVAILABLE_IPHONES
        );
        $this->objectManager->persist($iphonesAvailable);

        $ipadAvailable = new PhysicalPrizeAvailable(
            false,
            'iPad',
            static::COUNT_AVAILABLE_IPAD
        );
        $this->objectManager->persist($ipadAvailable);

        $macBookAvailable = new PhysicalPrizeAvailable(
            false,
            'macBook',
            static::COUNT_AVAILABLE_MACBOOK
        );
        $this->objectManager->persist($macBookAvailable);

        $this->objectManager->flush();
    }

    private function setUser( $class, $filedInTableContainUser )
    {
        $user = new User(
            'test_user@example.com',
            BankOfAmericaBanking::BANK_ALIAS
        );
        $user->setPassword('123');

        $this->objectManager->persist($user);
        $this->objectManager->flush();

        $this->mocServiceProperty( $class, $filedInTableContainUser, $user );
    }

    private function mocRandomizerWithNoAvailablePrize( $genetartor )
    {

        $noAvailablePrizeRepositoryMoc = $this->createMock( PhysicalPrizeSimpleRandomizer::class );
        $noAvailablePrizeRepositoryMoc->expects($this->any())
                                      ->method('findRandomPrize')
                                      ->willReturn( null );

        $this->mocServiceProperty( $genetartor, 'physicalPrizeAvailableRepository', $noAvailablePrizeRepositoryMoc );
    }

    /**
     * Проверяем положительный кейс генерации денежного приза
     *
     * Ожидаемый результат:
     * вернулся объект типа PhysicalPrize
     * создалась транзакция выданного количества денежного приза
     *
     * @throws \App\Exception\AvailablePhysicalPrizeNotFoundException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testGenerate()
    {
        /** @var PhysicalPrize $prizeGenerator */
        $prizeGenerator = $this->containerLocal->get( PhysicalPrize::class );
        $this->loadAvailablePhysicalPrize(  );
        $this->setUser( $prizeGenerator, 'user' );

        /** @var Prize $prize */
        $prize = $prizeGenerator->generate(  PhysicalPrize::PRIZE_TYPE_NAME );

        $this->assertTrue( $prize instanceof Prize);

        $transactions = $this->objectManager->getRepository(PhysicalPrizeTransaction::class)->findAll();
        $this->assertEquals(1, count($transactions) );

        /** @var PhysicalPrizeTransaction$transaction */
        $transaction = array_shift($transactions);

        $this->assertTrue($transaction instanceof PhysicalPrizeTransaction );
        $this->assertEquals(PhysicalPrizeTransaction::STATUS_ACCRUAL, $transaction->getStatus() );
        $this->assertEquals( $prizeGenerator->getUser(), $transaction->getUser() );

        $availablePrizes = $this->objectManager->getRepository(PhysicalPrizeAvailable::class )
                                              ->findAllAvailablePrizes();
        $this->assertEquals( 1, count( $availablePrizes ) );

        /** @var PhysicalPrizeAvailable $availablePrize */
        $availablePrize =  array_shift( $availablePrizes );
        $this->objectManager->refresh( $availablePrize );
        //  проверяем то доусупное количество изменилось
        $newAvailablePrizeCount = static::COUNT_AVAILABLE_IPHONES - 1;
        $this->assertEquals( $newAvailablePrizeCount, $availablePrize->getCount() );
    }

    /**
     * Проверяем что при не верном имени сервиса возващается null
     *
     * Ожидаемый результат:
     *   возвращается null
     */
    public function testIncorrectGeneratorName()
    {
        $prizeGenerator = $this->containerLocal->get( PhysicalPrize::class );
        $this->setUser( $prizeGenerator, 'user' );

        /** @var Prize|null $prize */
        $prize = $prizeGenerator->generate( 'incorrect service generator name' );

        $this->assertNull( $prize );
    }

    /**
     * Проверяем что при отсутствии доступных для розыгрыша физических призов возвращается null
     *
     * Ожидаемый результат: null
     *
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testNoAvailablePrize()
    {
        $prizeGenerator = $this->containerLocal->get( PhysicalPrize::class );
        $this->mocRandomizerWithNoAvailablePrize( $prizeGenerator );

        $prize = $prizeGenerator->generate(  PhysicalPrize::PRIZE_TYPE_NAME );

        $this->assertNull( $prize );
    }


}