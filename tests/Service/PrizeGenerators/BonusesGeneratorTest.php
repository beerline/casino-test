<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 01.12.2018
 * Time: 15:31
 */

namespace App\Tests\PrizeGenerators;

use App\Entity\BonusesAvailable;
use App\Entity\BonusTransaction;
use App\Entity\User;
use App\Exception\BonusRangeNotFoundException;
use App\GameEntity\BonusPrize;
use App\Repository\BonusesAvailableRepository;
use App\Service\Banking\BankOfAmericaBanking;
use App\Service\PrizeGenerators\Bonuses;
use App\Service\PrizeGenerators\Randomizer\Bonuses\BonusesSimpleRandomizer;
use App\Tests\AbstractCasinoServicesTest;
use App\Type\Decimal;

class BonusesGeneratorTest extends AbstractCasinoServicesTest
{
    const RANGE_MIN = 100;
    const RANGE_MAX = 1000;

    const OVER_MAX_RANGE = 1001;

    const LESS_THEN_MIN_RANGE = 99;

    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * Мокаем объекты через рефлексию что бы сохранить иммутабельность объектов
     *
     * @param $class
     */
    private function mocRepository( $class )
    {
        $bonusesAvailable = new BonusesAvailable(
            new Decimal(static::RANGE_MIN),
            new Decimal( static::RANGE_MAX )
        );

        $bonusesAvailableRepositoryMoc = $this->createMock( BonusesAvailableRepository::class );
        $bonusesAvailableRepositoryMoc->expects($this->any())
                                      ->method('findRange')
                                      ->willReturn($bonusesAvailable);

        $this->mocServiceProperty( $class, 'bonusesAvailableRepositorey', $bonusesAvailableRepositoryMoc );
    }

    /**
     * @param $class
     * @param $filedInTableContainUser
     */
    private function setUser( $class, $filedInTableContainUser )
    {
        $user = new User(
            'test_user@example.com',
            BankOfAmericaBanking::BANK_ALIAS
        );
        $user->setPassword('123');


        $this->objectManager->persist($user);
        $this->objectManager->flush();

        $this->mocServiceProperty( $class, $filedInTableContainUser, $user );
    }

    private function mocRandomizerOverMax( $class )
    {
        $randomizerMoc = $this->createMock( BonusesSimpleRandomizer::class );
        $randomizerMoc->expects( $this->any() )
                      ->method( 'random' )
                      ->willReturn( new Decimal( static::OVER_MAX_RANGE ) );

        $this->mocServiceProperty( $class, 'randomizer', $randomizerMoc);
    }

    private function mocRandomizerLessThenMin( $class )
    {
        $randomizerMoc = $this->createMock( BonusesSimpleRandomizer::class );
        $randomizerMoc->expects( $this->any() )
                      ->method( 'random' )
                      ->willReturn( new Decimal( static::LESS_THEN_MIN_RANGE ) );

        $this->mocServiceProperty( $class, 'randomizer', $randomizerMoc);
    }

        /**
     * Проверяем позитивный кейс генерации бонусов
     *
     * Ожидаемый результат:
     * вернулся объект типа BonusPrize
     * создалась транзакция выданного количества ,онусов
     */
    public function testGenerate()
    {
        /** @var Bonuses $prizeGenerator */
        $prizeGenerator = $this->containerLocal->get( Bonuses::class );
        $this->mocRepository( $prizeGenerator );
        $this->setUser( $prizeGenerator, 'user' );

        /** @var BonusPrize $prize */
        $prize = $prizeGenerator->generate(  Bonuses::PRIZE_TYPE_NAME );

        $this->assertTrue( $prize instanceof BonusPrize);
        $this->assertGreaterThanOrEqual( static::RANGE_MIN, $prize->getAmount()->toFloat() );
        $this->assertLessThanOrEqual( static::RANGE_MAX, $prize->getAmount()->toFloat() );

        $transactions = $this->objectManager->getRepository(BonusTransaction::class)->findAll();
        $this->assertEquals(1, count($transactions) );

        /** @var BonusTransaction $transaction */
        $transaction = array_shift($transactions);

        $this->assertTrue($transaction instanceof BonusTransaction );
        $this->assertEquals(BonusTransaction::TYPE_ACCRUAL, $transaction->getType() );
        $this->assertEquals( $prize->getAmount(), $transaction->getAmount() );
        $this->assertEquals( $prizeGenerator->getUser(), $transaction->getUser() );
    }

    /**
     * Проверяем что при отсутствии записей в таблице bonusesAvailable
     * возвращается null
     *
     * Ожидаемый результат: null
     */
    public function testNoBonusesAvailable()
    {
        $prizeGenerator = $this->containerLocal->get( Bonuses::class );
        $this->setUser( $prizeGenerator, 'user' );

        /** @var BonusPrize $prize */
        $prize = $prizeGenerator->generate( Bonuses::PRIZE_TYPE_NAME );
        $this->assertNull( $prize );
    }

    /**
     * Проверяем что при не верном имени сервиса возващается null
     *
     * Ожидаемый результат:
     *   возвращается null
     */
    public function testIncorrectGeneratorName()
    {
        $prizeGenerator = $this->containerLocal->get( Bonuses::class );
        $this->setUser( $prizeGenerator, 'user' );

        /** @var BonusPrize|null $prize */
        $prize = $prizeGenerator->generate( 'incorrect service generator name' );

        $this->assertNull( $prize );
    }

    /**
     * Проверяем что при генерации суммы бонусов больше установленного возвращается null
     *
     * Ожидаемый результат null
     */
    public function testOverMaxRangeGeneration()
    {
        /** @var Bonuses $prizeGenerator */
        $prizeGenerator = $this->containerLocal->get( Bonuses::class );
        $this->mocRepository( $prizeGenerator );
        $this->mocRandomizerOverMax( $prizeGenerator );
        $this->setUser( $prizeGenerator, 'user' );

        $prize = $prizeGenerator->generate( Bonuses::PRIZE_TYPE_NAME );
        $this->assertNull( $prize );
    }

    /**
     * Проверяем что при генерации суммы бонусов меньше установленного возвращается null
     *
     * Ожидаемый результат null
     */
    public function testLessThenMinRangeGeneration()
    {
        /** @var Bonuses $prizeGenerator */
        $prizeGenerator = $this->containerLocal->get( Bonuses::class );
        $this->mocRepository( $prizeGenerator );
        $this->mocRandomizerLessThenMin( $prizeGenerator );
        $this->setUser( $prizeGenerator, 'user' );

        $prize = $prizeGenerator->generate( Bonuses::PRIZE_TYPE_NAME );
        $this->assertNull( $prize );
    }
}