<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 02.12.2018
 * Time: 13:38
 */

namespace App\Tests\Service\PrizeGenerators;

use App\Entity\MoneyAvailable;
use App\Entity\MoneyTransaction;
use App\Entity\User;
use App\Exception\MoneyAvailableNotFoundException;
use App\GameEntity\MoneyPrize;
use App\Service\Banking\BankOfAmericaBanking;
use App\Service\PrizeGenerators\Money;
use App\Service\PrizeGenerators\Randomizer\Money\MoneySimpleRandomizer;
use App\Tests\AbstractCasinoServicesTest;
use App\Type\Decimal;

class MoneyGeneratorTest  extends AbstractCasinoServicesTest
{
    const RANGE_MIN = 100;
    const RANGE_MAX = 1000;
    const NOMINAL = 10000;
    const OVER_NOMINAL = 10001; // значение превышающее наминально дуступную сумму
    const OVER_MAX_RANGE = 1001; // значение превышающее максимальное
    const LESS_THEN_MIN_RANGE = 99; // значение меньше минимального

    protected function setUp()
    {
        parent::setUp();
    }

    private function loadAvailableMoney(  )
    {
        $moneyAvailable = new MoneyAvailable(
            new Decimal( static::NOMINAL ),
            new Decimal( static::RANGE_MIN ),
            new Decimal( static::RANGE_MAX )
        );

        $this->objectManager->persist($moneyAvailable);
        $this->objectManager->flush();
    }

    private function loadAvailableZeroMoney(  )
    {
        $moneyAvailable = new MoneyAvailable(
            new Decimal( 0 ),
            new Decimal( static::RANGE_MIN ),
            new Decimal( static::RANGE_MAX )
        );

        $this->objectManager->persist($moneyAvailable);
        $this->objectManager->flush();
    }

    private function mocRandomizerOverMax( $class )
    {
        $randomizerMoc = $this->createMock( MoneySimpleRandomizer::class );
        $randomizerMoc->expects( $this->any() )
            ->method( 'random' )
            ->willReturn( new Decimal( static::OVER_MAX_RANGE ) );

        $this->mocServiceProperty( $class, 'randomizer', $randomizerMoc);
    }

    private function mocRandomizerOverNominal( $class )
    {
        $randomizerMoc = $this->createMock( MoneySimpleRandomizer::class );
        $randomizerMoc->expects( $this->any() )
            ->method( 'random' )
            ->willReturn( new Decimal( static::OVER_NOMINAL ) );

        $this->mocServiceProperty( $class, 'randomizer', $randomizerMoc);
    }

    private function mocRandomizerLessThenMin( $class )
    {
        $randomizerMoc = $this->createMock( MoneySimpleRandomizer::class );
        $randomizerMoc->expects( $this->any() )
            ->method( 'random' )
            ->willReturn( new Decimal( static::LESS_THEN_MIN_RANGE ) );

        $this->mocServiceProperty( $class, 'randomizer', $randomizerMoc);
    }

    /**
     * @param $class
     * @param $filedInTableContainUser
     */
    private function setUser( $class, $filedInTableContainUser )
    {
        $user = new User(
            'test_user@example.com',
            BankOfAmericaBanking::BANK_ALIAS
        );
        $user->setPassword('123');


        $this->objectManager->persist($user);
        $this->objectManager->flush();

        $this->mocServiceProperty( $class, $filedInTableContainUser, $user );
    }

    /**
     * Проверяем положительный кейс генерации денежного приза
     *
     * Ожидаемый результат:
     * вернулся объект типа MoneyPrize
     * создалась транзакция выданного количества денежного приза
     *
     * @throws \App\Exception\MoneyAvailableNotFoundException
     * @throws \Doctrine\ORM\TransactionRequiredException
     * @throws \Psr\Container\ContainerExceptionInterface
     * @throws \Psr\Container\NotFoundExceptionInterface
     */
    public function testGenerate()
    {
        /** @var Money $prizeGenerator */
        $prizeGenerator = $this->containerLocal->get( Money::class );
        $this->loadAvailableMoney(  );
        $this->setUser( $prizeGenerator, 'user' );

        /** @var MoneyPrize $prize */
        $prize = $prizeGenerator->generate(  Money::PRIZE_TYPE_NAME );

        $this->assertTrue( $prize instanceof MoneyPrize);
        $this->assertGreaterThanOrEqual( static::RANGE_MIN, $prize->getAmount()->toFloat() );
        $this->assertLessThanOrEqual( static::RANGE_MAX, $prize->getAmount()->toFloat() );
        $this->assertLessThanOrEqual( static::NOMINAL, $prize->getAmount()->toFloat() );

        $transactions = $this->objectManager->getRepository(MoneyTransaction::class)->findAll();
        $this->assertEquals(1, count($transactions) );

        /** @var MoneyTransaction $transaction */
        $transaction = array_shift($transactions);

        $this->assertTrue($transaction instanceof MoneyTransaction );
        $this->assertEquals(MoneyTransaction::TYPE_ACCRUAL, $transaction->getType() );
        $this->assertEquals( $prize->getAmount(), $transaction->getAmount() );
        $this->assertEquals( $prizeGenerator->getUser(), $transaction->getUser() );

        /** @var MoneyAvailable $availableMoney */
        $availableMoney = $this->objectManager->getRepository(MoneyAvailable::class )->findOneBy([]);
        $this->objectManager->refresh( $availableMoney );
        $nominalAndAvailableDiff = static::NOMINAL - $availableMoney->getSum()->toFloat();
        $this->assertEquals( $nominalAndAvailableDiff, $prize->getAmount()->toFloat() );
        $this->assertEquals( $nominalAndAvailableDiff, $transaction->getAmount()->toFloat() );
    }

    /**
     * Проверяем что при отсутствии записей в таблице money§Available
     * возвращаем null
     *
     * Ожидаемый результат: null
     */
    public function testNoMoneyAvailable()
    {
        $prizeGenerator = $this->containerLocal->get( Money::class );
        $this->setUser( $prizeGenerator, 'user' );

        /** @var MoneyPrize $prize */
        $prize = $prizeGenerator->generate( Money::PRIZE_TYPE_NAME );
        $this->assertNull( $prize );
    }

    /**
     * Проверяем что при не верном имени сервиса возващается null
     *
     * Ожидаемый результат:
     *   возвращается null
     */
    public function testIncorrectGeneratorName()
    {
        $prizeGenerator = $this->containerLocal->get( Money::class );
        $this->setUser( $prizeGenerator, 'user' );

        /** @var MoneyPrize|null $prize */
        $prize = $prizeGenerator->generate( 'incorrect service generator name' );

        $this->assertNull( $prize );
    }

    /**
     * Проверяем что при нулевом остатке денежных средств для генерации получаем null
     *
     * Ожидаемый результат:
     *   возвращается null
     */
    public function testAvailableMoneyIsZero()
    {
        /** @var Money $prizeGenerator */
        $prizeGenerator = $this->containerLocal->get( Money::class );
        $this->loadAvailableZeroMoney( $prizeGenerator );
        $this->setUser( $prizeGenerator, 'user' );

        $prize = $prizeGenerator->generate( Money::PRIZE_TYPE_NAME );
        $this->assertNull( $prize );
    }

    /**
     * Проверяем что при генерации суммы денежного приза больше установленного возвращается null
     *
     * Ожидаемый результат null
     */
    public function testOverNominalGeneration()
    {
        /** @var Money $prizeGenerator */
        $prizeGenerator = $this->containerLocal->get( Money::class );
        $this->loadAvailableMoney( $prizeGenerator );
        $this->mocRandomizerOverNominal( $prizeGenerator );
        $this->setUser( $prizeGenerator, 'user' );

        $prize = $prizeGenerator->generate( Money::PRIZE_TYPE_NAME );
        $this->assertNull( $prize );
    }

    /**
     * Проверяем что при генерации суммы денежного приза больше установленного возвращается null
     *
     * Ожидаемый результат null
     */
    public function testOverMaxRangeGeneration()
    {
        /** @var Money $prizeGenerator */
        $prizeGenerator = $this->containerLocal->get( Money::class );
        $this->loadAvailableMoney( $prizeGenerator );
        $this->mocRandomizerOverMax( $prizeGenerator );
        $this->setUser( $prizeGenerator, 'user' );

        $prize = $prizeGenerator->generate( Money::PRIZE_TYPE_NAME );
        $this->assertNull( $prize );
    }

    /**
     * Проверяем что при генерации суммы денежного приза меньше установленного возвращается null
     *
     * Ожидаемый результат null
     */
    public function testLessThenMinRangeGeneration()
    {
        /** @var Money $prizeGenerator */
        $prizeGenerator = $this->containerLocal->get( Money::class );
        $this->loadAvailableMoney( $prizeGenerator );
        $this->mocRandomizerLessThenMin( $prizeGenerator );
        $this->setUser( $prizeGenerator, 'user' );

        $prize = $prizeGenerator->generate( Money::PRIZE_TYPE_NAME );
        $this->assertNull( $prize );
    }
}