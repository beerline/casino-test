<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 02.12.2018
 * Time: 20:14
 */

namespace App\Tests\Service\PrizeGenerators\Randomizer;

use App\Entity\PhysicalPrizeAvailable;
use App\Repository\PhysicalPrizeAvailableRepository;
use App\Service\PrizeGenerators\Randomizer\PhysicalPrize\PhysicalPrizeSimpleRandomizer;
use App\Tests\AbstractCasinoServicesTest;

class PhysicalPrizeSimpleRandomizerTest extends AbstractCasinoServicesTest
{

    /**
     * @param $randomizer
     */
    private function mocAvailablePrizeRepository( $randomizer )
    {
        $prize1 = new PhysicalPrizeAvailable(
            true,
            'iPad',
            100
        );
        $availablePrizes[] = $prize1;

        $prize2 = new PhysicalPrizeAvailable(
            true,
            'iPad',
            100
        );
        $availablePrizes[] = $prize2;

        $prize3 = new PhysicalPrizeAvailable(
            true,
            'iPad',
            100
        );
        $availablePrizes[] = $prize3;

        $availablePrizeRepositoryMoc = $this->createMock( PhysicalPrizeAvailableRepository::class );
        $availablePrizeRepositoryMoc->expects($this->any())
                                      ->method('findAllAvailablePrizes')
                                      ->willReturn( $availablePrizes );

        $this->mocServiceProperty( $randomizer, 'physicalPrizeAvailableRepository', $availablePrizeRepositoryMoc );
    }

    private function mocNoAvailablePrizeRepository( $randomizer )
    {
        $availablePrizeRepositoryMoc = $this->createMock( PhysicalPrizeAvailableRepository::class );
        $availablePrizeRepositoryMoc->expects($this->any())
                                    ->method('findAllAvailablePrizes')
                                    ->willReturn( null );

        $this->mocServiceProperty( $randomizer, 'physicalPrizeAvailableRepository', $availablePrizeRepositoryMoc );
    }


    /**
     * Позитивный кей.
     *
     * Ожидаемый результат:
     *  один элемент типа PhysicalPrizeAvailable
     */
    public function testSuccessFindingPhysicalPrize()
    {
        /** @var PhysicalPrizeSimpleRandomizer $randomizer */
        $randomizer = $this->containerLocal->get( PhysicalPrizeSimpleRandomizer::class );
        $this->mocAvailablePrizeRepository( $randomizer );
        $prize = $randomizer->findRandomPrize();

        $this->assertTrue( $prize instanceof PhysicalPrizeAvailable);
    }

    public function testNoAvailablePrizes()
    {
        /** @var PhysicalPrizeSimpleRandomizer $randomizer */
        $randomizer = $this->containerLocal->get( PhysicalPrizeSimpleRandomizer::class );
        $this->mocNoAvailablePrizeRepository( $randomizer );
        $prize = $randomizer->findRandomPrize();

        $this->assertNull( $prize );
    }


}