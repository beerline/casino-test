<?php

namespace App\Tests;

use App\Service\PrizeGenerator;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 22:51
 */

class PrizeGeneratorTest extends WebTestCase
{

    public function testGenerator()
    {
        self::bootKernel([]);

        $container = self::$container;
        $prizeGenerator = $container->get(PrizeGenerator::class);
        $prizeGenerator->handle();
    }
}