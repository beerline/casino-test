<?php

namespace App\Tests;

use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\Common\Persistence\ObjectManager;
use Psr\Container\ContainerInterface;
use ReflectionClass;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\HttpKernel\KernelInterface;

/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 01.12.2018
 * Time: 15:37
 */

class AbstractCasinoServicesTest extends KernelTestCase
{
    /**
     * @var KernelInterface
     */
    protected $kernelLocal;

    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * @var ContainerInterface
     */
    protected $containerLocal;

    protected function setUp()
    {
        $this->kernelLocal = self::bootKernel();
        $this->containerLocal = self::$container;
        $this->objectManager = $this->kernelLocal->getContainer()
                                                 ->get('doctrine')
                                                 ->getManager();
        $this->pureDB();
    }

    /**
     * Мокаем сово объекта
     */
    protected function mocServiceProperty( $class, $propertyName, $moc )
    {
        $reflectedClass = new ReflectionClass( $class );
        $prop = $reflectedClass->getProperty($propertyName);
        $prop->setAccessible(true);
        $prop->setValue( $class, $moc );
    }

    protected function tearDown()
    {
        parent::tearDown();

        $this->objectManager->close();
        $this->objectManager = null; // avoid memory leaks
    }

    private function pureDB()
    {
        $purger = new ORMPurger( $this->objectManager );
        $purger->purge(  );
    }
}