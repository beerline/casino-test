<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 19:57
 */

namespace App\Hdrator;

use App\Hdrator\Entity\MoneyTransactionForBanking;

interface MoneyTransactionForBankingHydratorInterface
{
    /**
     * Заполняем объект сырыми данными из БД
     *
     * @param array $array
     *
     * @return MoneyTransactionForBanking
     */
    public function hydrateSingleObject( array $array ) : MoneyTransactionForBanking;

    /**
     * @param array $elements
     *
     * @return MoneyTransactionForBanking[]
     */
    public function hydrateArrayObjects( array $elements) : array ;
}