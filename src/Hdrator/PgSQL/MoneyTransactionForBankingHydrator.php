<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 19:57
 */

namespace App\Hdrator\PgSQL;

use App\Entity\User;
use App\Hdrator\AbstractMoneyTransactionForBankingHydrator;
use App\Hdrator\Entity\MoneyTransactionForBanking;
use App\Hdrator\Exception\AmountForBankingNotFoundException;
use App\Hdrator\Exception\UserNotFoundException;
use App\Repository\UserRepository;
use App\Type\Decimal;

class MoneyTransactionForBankingHydrator extends AbstractMoneyTransactionForBankingHydrator
{
    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * MoneyTransactionForBankingHydrator constructor.
     *
     * @param UserRepository $userRepository
     */
    public function __construct( UserRepository $userRepository )
    {
        $this->userRepository = $userRepository;
    }

    /**
     * Заполняем объект сырыми данными из БД
     *
     * @param array $array
     *
     * @return MoneyTransactionForBanking
     *
     * @throws AmountForBankingNotFoundException
     * @throws UserNotFoundException
     */
    public function hydrateSingleObject( array $array ): MoneyTransactionForBanking
    {
        $user = $this->userRepository->findOneBy( [ "id" => $array[ static::HYDRATE_FIELD_NAME_USER] ] );

        if ( ! $user instanceof User) {
            throw new UserNotFoundException();
        }

        if ( empty( $array[ static::HYDRATE_FIELD_NAME_AMOUNT] ) ) {
            throw new AmountForBankingNotFoundException();
        }

        return ( new MoneyTransactionForBanking(
            $user,
            ( new Decimal( $array[ static::HYDRATE_FIELD_NAME_AMOUNT] ) )
        ) );
    }

    /**
     * @param array $elements
     *
     * @return MoneyTransactionForBanking[]
     *
     * @throws UserNotFoundException
     * @throws AmountForBankingNotFoundException
     */
    public function hydrateArrayObjects( array $elements ): array
    {
        $result = [];
        foreach ( $elements as  $element ) {
            $result[] = $this->hydrateSingleObject( $element );
        }

        return $result;
    }
}