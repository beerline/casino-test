<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 20:26
 */

namespace App\Hdrator;

abstract class AbstractMoneyTransactionForBankingHydrator implements MoneyTransactionForBankingHydratorInterface
{
    const HYDRATE_FIELD_NAME_AMOUNT = 'amount';
    const HYDRATE_FIELD_NAME_USER = 'usr';
}