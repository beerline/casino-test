<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 20:18
 */

namespace App\Hdrator\Exception;

class UserNotFoundException extends AbstractHydratorException
{
    protected $message = 'User not found';
}