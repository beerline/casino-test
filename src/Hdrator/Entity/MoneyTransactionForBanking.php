<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 20:01
 */

namespace App\Hdrator\Entity;

use App\Entity\User;
use App\Type\Decimal;

class MoneyTransactionForBanking
{

    /**
     * @var User
     */
    private $user;

    /**
     * @var Decimal
     */
    private $amount;

    /**
     * MoneyTransactionForBanking constructor.
     *
     * @param User    $user
     * @param Decimal $amount
     */
    public function __construct( User $user, Decimal $amount )
    {
        $this->user = $user;
        $this->amount = $amount;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return Decimal
     */
    public function getAmount(): Decimal
    {
        return $this->amount;
    }


}