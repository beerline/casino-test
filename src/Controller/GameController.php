<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 23.11.2018
 * Time: 20:21
 */

namespace App\Controller;

use App\Service\PhysicalPrizeRefuse;
use App\Service\PrizeGenerator;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;

class GameController extends AbstractCasinoController
{
    public function game(Request $request)
    {
        return $this->render('game/game.html.twig');
    }

    /**
     * Получаем слуайный приз
     *
     * @param PrizeGenerator  $prizeGenerator
     * @param LoggerInterface $logger
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getPrize( PrizeGenerator $prizeGenerator, LoggerInterface $logger )
    {
        $logger->info("Incoming request for generate prize for " . $this->getUser()->getEmail());
        $response = $this->businessLogicRequestHandle($prizeGenerator, $logger);

        return $response;
    }

    /**
     * Отках от приза
     *
     * @param PhysicalPrizeRefuse $physicalPrizeRefuse
     * @param Request             $request
     * @param LoggerInterface     $logger
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function refusePhysicalPrize(PhysicalPrizeRefuse $physicalPrizeRefuse, Request $request, LoggerInterface $logger)
    {
        $logger->info("Incoming request for reject a prize " . $this->getUser()->getEmail());
        $physicalPrizeRefuse->setRequest( $request );

        $response = $this->businessLogicRequestHandle($physicalPrizeRefuse, $logger);

        return $response;
    }
}