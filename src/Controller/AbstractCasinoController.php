<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 1:10
 */

namespace App\Controller;

use App\Exception\AbstractCasinoException;
use App\Service\BusinessLogicServiceInterface;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractCasinoController extends AbstractController
{
    /**
     * Метод для запросов бизнесслогики
     *
     * @param BusinessLogicServiceInterface $service
     * @param LoggerInterface               $logger
     *
     * @return Response
     */
    public function businessLogicRequestHandle( BusinessLogicServiceInterface $service, LoggerInterface $logger )
    {
        try {
            return $service->handle();

        } catch ( AbstractCasinoException $e ) {
            $logger->warning($e->getMessage());

            return new Response( $e->getResponseHttpCode());
        } catch ( \Exception $e ) {
            $logger->warning($e->getMessage());

            return new Response( Response::HTTP_INTERNAL_SERVER_ERROR);
        }
    }
}