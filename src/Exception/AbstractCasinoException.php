<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 1:09
 */

namespace App\Exception;

abstract class AbstractCasinoException extends \Exception implements CasinoException
{
    /**
     * код возвращаемый в ответе
     *
     * @var int
     */
    protected $responseHttpCode;

    /**
     * AbstractCasinoException constructor.
     *
     * @param int    $responseHttpCode
     * @param string $message
     * @param int    $errorCode
     */
    public function __construct( int $responseHttpCode, string $message = '', int $errorCode = 0)
    {
        $this->responseHttpCode = $responseHttpCode;
    }

    /**
     * @return int
     */
    public function getResponseHttpCode(): int
    {
        return $this->responseHttpCode;
    }
}