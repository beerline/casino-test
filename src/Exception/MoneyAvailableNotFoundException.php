<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 13:06
 */

namespace App\Exception;

class MoneyAvailableNotFoundException extends AbstractCasinoException
{
    protected $message = 'Money available not found';
}