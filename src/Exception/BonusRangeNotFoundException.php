<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 1:24
 */

namespace App\Exception;

class BonusRangeNotFoundException extends AbstractCasinoException
{
    protected $message = 'BonusRange not found';
}