<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 15:44
 */

namespace App\Exception;

class AvailablePhysicalPrizeNotFoundException extends AbstractCasinoException
{
    protected $message = 'Available physical prise not found';
}