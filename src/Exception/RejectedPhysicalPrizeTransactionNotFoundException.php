<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 21:55
 */

namespace App\Exception;

class RejectedPhysicalPrizeTransactionNotFoundException extends AbstractCasinoException
{
    protected $message = 'Rejected Physical Prize Transaction Not Found';
}