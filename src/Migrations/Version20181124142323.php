<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181124142323 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE money_available ALTER sum TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE bonus_transaction ALTER amount TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE bonus_transaction ALTER amount DROP DEFAULT');
        $this->addSql('ALTER TABLE money_transaction ALTER amount TYPE NUMERIC(10, 2)');
        $this->addSql('ALTER TABLE money_transaction ALTER amount DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE bonus_transaction ALTER amount TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE bonus_transaction ALTER amount DROP DEFAULT');
        $this->addSql('ALTER TABLE money_transaction ALTER amount TYPE DOUBLE PRECISION');
        $this->addSql('ALTER TABLE money_transaction ALTER amount DROP DEFAULT');
        $this->addSql('ALTER TABLE money_available ALTER sum TYPE NUMERIC(7, 2)');
    }
}
