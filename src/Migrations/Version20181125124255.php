<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181125124255 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE physical_prize_transaction ADD rejected_transaction_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE physical_prize_transaction ADD CONSTRAINT FK_CE5757B072E9D293 FOREIGN KEY (rejected_transaction_id) REFERENCES physical_prize_transaction (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_CE5757B072E9D293 ON physical_prize_transaction (rejected_transaction_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE physical_prize_transaction DROP CONSTRAINT FK_CE5757B072E9D293');
        $this->addSql('DROP INDEX UNIQ_CE5757B072E9D293');
        $this->addSql('ALTER TABLE physical_prize_transaction DROP rejected_transaction_id');
    }
}
