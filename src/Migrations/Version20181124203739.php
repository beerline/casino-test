<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181124203739 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bonuses_available_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bonuses_available (id INT NOT NULL, min_generate_range NUMERIC(10, 2) NOT NULL, max_generate_range NUMERIC(10, 2) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('ALTER TABLE money_available ALTER min_generate_range DROP DEFAULT');
        $this->addSql('ALTER TABLE money_available ALTER max_generate_range DROP DEFAULT');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('DROP SEQUENCE bonuses_available_id_seq CASCADE');
        $this->addSql('DROP TABLE bonuses_available');
        $this->addSql('ALTER TABLE money_available ALTER min_generate_range SET DEFAULT \'1\'');
        $this->addSql('ALTER TABLE money_available ALTER max_generate_range SET DEFAULT \'1\'');
    }
}
