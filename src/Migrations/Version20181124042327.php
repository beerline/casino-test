<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181124042327 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE bonus_transaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE physical_prize_transaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE SEQUENCE money_transaction_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE bonus_transaction (id INT NOT NULL, user_id INT DEFAULT NULL, date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type VARCHAR(255) NOT NULL, amount DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_487D3D7DA76ED395 ON bonus_transaction (user_id)');
        $this->addSql('CREATE TABLE physical_prize_transaction (id INT NOT NULL, user_id INT DEFAULT NULL, date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, status VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_CE5757B0A76ED395 ON physical_prize_transaction (user_id)');
        $this->addSql('CREATE TABLE money_transaction (id INT NOT NULL, user_id INT DEFAULT NULL, date_time TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, type VARCHAR(255) NOT NULL, amount DOUBLE PRECISION NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_D21254E2A76ED395 ON money_transaction (user_id)');
        $this->addSql('ALTER TABLE bonus_transaction ADD CONSTRAINT FK_487D3D7DA76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE physical_prize_transaction ADD CONSTRAINT FK_CE5757B0A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE money_transaction ADD CONSTRAINT FK_D21254E2A76ED395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE bonus_transaction DROP CONSTRAINT FK_487D3D7DA76ED395');
        $this->addSql('ALTER TABLE physical_prize_transaction DROP CONSTRAINT FK_CE5757B0A76ED395');
        $this->addSql('ALTER TABLE money_transaction DROP CONSTRAINT FK_D21254E2A76ED395');
        $this->addSql('DROP SEQUENCE bonus_transaction_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE physical_prize_transaction_id_seq CASCADE');
        $this->addSql('DROP SEQUENCE money_transaction_id_seq CASCADE');
        $this->addSql('DROP TABLE bonus_transaction');
        $this->addSql('DROP TABLE physical_prize_transaction');
        $this->addSql('DROP TABLE money_transaction');
    }
}
