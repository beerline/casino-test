<?php declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181125180344 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE physical_prize_transaction ADD physical_prize_available_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE physical_prize_transaction DROP name');
        $this->addSql('ALTER TABLE physical_prize_transaction ADD CONSTRAINT FK_CE5757B0CDACA3E FOREIGN KEY (physical_prize_available_id) REFERENCES physical_prize_available (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_CE5757B0CDACA3E ON physical_prize_transaction (physical_prize_available_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE physical_prize_transaction DROP CONSTRAINT FK_CE5757B0CDACA3E');
        $this->addSql('DROP INDEX IDX_CE5757B0CDACA3E');
        $this->addSql('ALTER TABLE physical_prize_transaction ADD name VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE physical_prize_transaction DROP physical_prize_available_id');
    }
}
