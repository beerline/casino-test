<?php

namespace App\Entity;

use App\Type\Decimal;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BonusesAvailableRepository")
 */
class BonusesAvailable
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * Минимальная сумма диапозона генерации приза
     *
     * @var Decimal
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $minGenerateRange;

    /**
     * Максимальная сумма диапозона генерации приза
     *
     * @var Decimal
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $maxGenerateRange;

    /**
     * BonusesAvailable constructor.
     *
     * @param Decimal $minGenerateRange
     * @param Decimal $maxGenerateRange
     */
    public function __construct( Decimal $minGenerateRange, Decimal $maxGenerateRange )
    {
        $this->minGenerateRange = $minGenerateRange;
        $this->maxGenerateRange = $maxGenerateRange;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Decimal
     */
    public function getMinGenerateRange(): Decimal
    {
        return (new Decimal( $this->minGenerateRange ) );
    }

    /**
     * @return Decimal
     */
    public function getMaxGenerateRange(): Decimal
    {
        return ( new Decimal( $this->maxGenerateRange ) );
    }

}
