<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Спсиок физических призов и их количество доступных для розыгрыша
 *
 * @ORM\Entity(repositoryClass="App\Repository\PhysicalPrizeAvailableRepository")
 */
class PhysicalPrizeAvailable
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $isAvailableForGame;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $count;

    /**
     * PhysicalPrizeAvailable constructor.
     *
     * @param bool   $isAvailableForGame
     * @param string $name
     * @param int    $count
     */
    public function __construct(bool $isAvailableForGame, string $name, int $count)
    {
        $this->isAvailableForGame = $isAvailableForGame;
        $this->name = $name;
        $this->count = $count;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isAvailableForGame(): bool
    {
        return $this->isAvailableForGame;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }
}
