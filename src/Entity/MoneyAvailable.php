<?php

namespace App\Entity;

use App\Type\Decimal;
use Doctrine\ORM\Mapping as ORM;

/**
 * Сумма доступная для розыгрыша всем игрокам
 *
 * @ORM\Entity(repositoryClass="App\Repository\MoneyAvailableRepository")
 */
class MoneyAvailable
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var Decimal
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     *
     * TODO переименовать в amount
     */
    private $sum;

    /**
     * Минимальная сумма диапозона генерации приза
     *
     * @var Decimal
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $minGenerateRange;

    /**
     * Максимальная сумма диапозона генерации приза
     *
     * @var Decimal
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $maxGenerateRange;

    /**
     * MoneyAvailable constructor.
     *
     * @param Decimal $sum
     * @param Decimal $minGenerateRange
     * @param Decimal $maxGenerateRange
     */
    public function __construct(Decimal $sum, Decimal $minGenerateRange, Decimal $maxGenerateRange)
    {
        $this->sum = $sum;
        $this->minGenerateRange = $minGenerateRange;
        $this->maxGenerateRange = $maxGenerateRange;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Decimal
     */
    public function getSum(): Decimal
    {
        return ( new Decimal( $this->sum ) );
    }

    /**
     * @return Decimal
     */
    public function getMinGenerateRange(): Decimal
    {
        return ( new Decimal( $this->minGenerateRange ) );
    }

    /**
     * @return Decimal
     */
    public function getMaxGenerateRange(): Decimal
    {
        return ( new Decimal( $this->maxGenerateRange ) );
    }
}
