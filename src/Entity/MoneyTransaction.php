<?php

namespace App\Entity;

use App\Type\Decimal;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Transactions\MoneyTransactionRepository")
 */
class MoneyTransaction
{
    const TYPE_ACCRUAL = 'accrual';
    const TYPE_CONVERSION_TO_BONUS = 'conversion_to_bonus';
    const TYPE_MOVED_TO_BANK_ACCOUNT = 'moved_to_bank_account';

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $dateTime;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255)
     */
    private $type;

    /**
     * @var Decimal
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="id"))
     */
    private $user;

    /**
     * MoneyTransaction constructor.
     *
     * @param string         $type
     * @param Decimal        $amount
     * @param UserInterface  $user
     * @param \DateTime|null $dateTime
     */
    public function __construct(string $type, Decimal $amount, UserInterface $user, \DateTime $dateTime = null)
    {
        $this->dateTime = $dateTime ?? new \DateTime();
        $this->type = $type;
        $this->amount = $amount;
        $this->user = $user;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return Decimal
     */
    public function getAmount(): Decimal
    {
        return ( new Decimal( $this->amount ) );
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }



}
