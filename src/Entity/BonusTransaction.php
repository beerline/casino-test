<?php

namespace App\Entity;


use App\Type\Decimal;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Transactions\BonusTransactionRepository")
 */
class BonusTransaction
{
    const TYPE_ACCRUAL = 'accrual';
    const TYPE_USE = 'use';
    const TYPE_MONEY_CONVERSION = 'money_conversion';

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer", unique=TRUE)
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $dateTime;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @var Decimal
     *
     * @ORM\Column(type="decimal", precision=10, scale=2)
     */
    private $amount;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="id")
     */
    private $user;

    /**
     * BonusTransaction constructor.
     *
     * @param string         $type
     * @param Decimal        $amount
     * @param UserInterface  $user
     * @param \DateTime|null $dateTime
     */
    public function __construct(string $type, Decimal $amount, UserInterface $user, \DateTime $dateTime = null )
    {
        $this->dateTime = $dateTime ?? new \DateTime();
        $this->type = $type;
        $this->amount = $amount;
        $this->user = $user;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @return Decimal
     */
    public function getAmount(): Decimal
    {
        return $this->amount;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }


}
