<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Таблица содержит в себе названия типов разыгрываемых призов/
 * С помощью поле enable можно включать и выключать доступночть типа приза для розыгрыша
 *
 * @ORM\Entity(repositoryClass="App\Repository\AvailablePrizeTypesRepository")
 */
class AvailablePrizeTypes
{
    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean")
     */
    private $enable;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $name;

    /**
     * AvailablePriceTypes constructor.
     *
     * @param bool   $enable
     * @param string $name
     */
    public function __construct(bool $enable, string $name)
    {
        $this->enable = $enable;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return bool
     */
    public function isEnable(): bool
    {
        return $this->enable;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }
}
