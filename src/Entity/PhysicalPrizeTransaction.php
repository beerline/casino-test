<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Transactions\PhysicalPrizeTransactionRepository")
 */
class PhysicalPrizeTransaction
{
    const STATUS_ACCRUAL = 'accrual';
    const STATUS_SHIPPED = 'shipped';
    const STATUS_REJECT  = 'rejected';

    /**
     * @var int
     *
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @ORM\Column(type="datetime")
     */
    private $dateTime;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $status;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="id")
     */
    private $user;

    /**
     * @var PhysicalPrizeAvailable
     *
     * @ORM\ManyToOne(targetEntity="PhysicalPrizeAvailable", inversedBy="id")
     */
    private $physicalPrizeAvailable;

    /**
     * Танзакция от котроый отказывается игрок
     *
     * @var PhysicalPrizeTransaction|null
     *
     * @ORM\OneToOne(targetEntity="PhysicalPrizeTransaction")
     */
    private $rejectedTransaction;

    /**
     * PhysicalPrizeTransaction constructor.
     *
     * @param string                        $status
     * @param User                          $user
     * @param PhysicalPrizeAvailable        $physicalPrizeAvailable
     * @param \DateTime|null                $dateTime
     * @param PhysicalPrizeTransaction|null $rejectedTransaction
     */
    public function __construct(
        string $status
        , User $user
        , PhysicalPrizeAvailable $physicalPrizeAvailable
        , \DateTime $dateTime = null
        , PhysicalPrizeTransaction $rejectedTransaction = null
    ) {
        $this->dateTime = $dateTime ?? new \DateTime();
        $this->status = $status;
        $this->user = $user;
        $this->rejectedTransaction = $rejectedTransaction;
        $this->physicalPrizeAvailable = $physicalPrizeAvailable;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return \DateTime
     */
    public function getDateTime(): \DateTime
    {
        return $this->dateTime;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @return PhysicalPrizeTransaction|null
     */
    public function getRejectedTransaction(): ?PhysicalPrizeTransaction
    {
        return $this->rejectedTransaction;
    }

    /**
     * @return PhysicalPrizeAvailable
     */
    public function getPhysicalPrizeAvailable(): PhysicalPrizeAvailable
    {
        return $this->physicalPrizeAvailable;
    }

}
