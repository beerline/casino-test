<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 02.12.2018
 * Time: 22:00
 */

namespace App\SerialiseEntity;

class GeneratePrizeResponse
{
    /**
     * Тип полученного приза
     *
     * @var string
     */
    private $type;

    /**
     * Сам приз: деньги, бонусы или материальный подарок
     *
     * @var string
     */
    private $prize;

    /**
     * GeneratePrizeResponse constructor.
     *
     * @param string $type
     * @param string $prize
     */
    public function __construct( string $type, string $prize )
    {
        $this->type = $type;
        $this->prize = $prize;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType( string $type ): void
    {
        $this->type = $type;
    }

    /**
     * @return string
     */
    public function getPrize(): string
    {
        return $this->prize;
    }

    /**
     * @param string $prize
     */
    public function setPrize( string $prize ): void
    {
        $this->prize = $prize;
    }
}