<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 23:30
 */

namespace App\Command;

use App\Exception\AbstractConsoleException;
use App\Service\SendMoneyToBank;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendMoneyPrizeToBank extends Command
{
    /**
     * @var SendMoneyToBank
     */
    private $sendMoneyToBankService;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * SendMoneyPrizeToBank constructor.
     *
     * @param SendMoneyToBank $sendMoneyToBankService
     * @param LoggerInterface $logger
     */
    public function __construct( SendMoneyToBank $sendMoneyToBankService, LoggerInterface $logger )
    {
        $this->sendMoneyToBankService = $sendMoneyToBankService;
        $this->logger = $logger;

        parent::__construct();
    }

    protected function configure()
    {
        $this
            ->setName('user:money_prize:send_to_bank')
            ->setDescription('Sending money prize to bank');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        echo "======== Sending money prize to bank ========\n\n";
        try {
            $this->sendMoneyToBankService->findAndSend();
        } catch ( AbstractConsoleException $e) {
            $this->logger->error( $e->getMessage() );
        } catch ( \Exception $e ) {
            $this->logger->error( $e->getMessage() );
        }
    }
}