<?php

namespace App\DataFixtures;

use App\Entity\BonusesAvailable;
use App\Type\Decimal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class BonusesAvailableFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $min = new Decimal(150);
        $max = new Decimal(200);
        $bonusesAvailable = new BonusesAvailable( $min, $max );

        $manager->persist($bonusesAvailable);
        $manager->flush();
    }
}
