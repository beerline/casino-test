<?php

namespace App\DataFixtures;

use App\Entity\AvailablePrizeTypes;
use App\Service\PrizeGenerators\Bonuses;
use App\Service\PrizeGenerators\Money;
use App\Service\PrizeGenerators\PhysicalPrize;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AvailablePrizeTypesFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $availablePrizeTypes = new AvailablePrizeTypes(
            true,
            Bonuses::getGeneratorTypeName()
        );
        $manager->persist($availablePrizeTypes);

        $availablePrizeTypes = new AvailablePrizeTypes(
            true,
            Money::getGeneratorTypeName()
        );
        $manager->persist($availablePrizeTypes);

        $availablePrizeTypes = new AvailablePrizeTypes(
            true,
            PhysicalPrize::getGeneratorTypeName()
        );
        $manager->persist($availablePrizeTypes);


        $manager->flush();
    }
}
