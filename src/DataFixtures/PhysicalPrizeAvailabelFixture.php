<?php

namespace App\DataFixtures;

use App\Entity\PhysicalPrizeAvailable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class PhysicalPrizeAvailabelFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $physicalPrize = new PhysicalPrizeAvailable(
            true,
            'car',
            100
        );
        $manager->persist($physicalPrize);

        $physicalPrize = new PhysicalPrizeAvailable(
            false,
            'house',
            150
        );
        $manager->persist($physicalPrize);

        $physicalPrize = new PhysicalPrizeAvailable(
            true,
            'iPhone',
            1000
        );
        $manager->persist($physicalPrize);

        $physicalPrize = new PhysicalPrizeAvailable(
            false,
            'iPad',
            2000
        );
        $manager->persist($physicalPrize);

        $physicalPrize = new PhysicalPrizeAvailable(
            true,
            'bike',
            1500
        );
        $manager->persist($physicalPrize);


        $physicalPrize = new PhysicalPrizeAvailable(
            true,
            'macBook',
        0
        );
        $manager->persist($physicalPrize);



        $manager->flush();
    }
}
