<?php

namespace App\DataFixtures;

use App\Entity\MoneyAvailable;
use App\Type\Decimal;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class MoneyAvailableFixture extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $decimal = new Decimal(100001.155);
        $min = new Decimal(10);
        $max = new Decimal(100);
        $moneyAvailable = new MoneyAvailable($decimal, $min, $max);

        $manager->persist($moneyAvailable);
        $manager->flush();
    }
}
