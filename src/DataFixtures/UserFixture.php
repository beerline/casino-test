<?php

namespace App\DataFixtures;

use App\Entity\User;
use App\Service\Banking\BankOfAmericaBanking;
use App\Service\Banking\SberBankBanking;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class UserFixture extends Fixture
{
    private $passwordEncoder;

    public function __construct(UserPasswordEncoderInterface $passwordEncoder)
     {
         $this->passwordEncoder = $passwordEncoder;
     }

    public function load(ObjectManager $manager)
    {
        $user = new User('albert.einsteine@gamil.com', BankOfAmericaBanking::BANK_ALIAS );
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            '123'
        ));
        $manager->persist($user);

        $user = new User('tesla@gamil.com', SberBankBanking::BANK_ALIAS );
        $user->setPassword($this->passwordEncoder->encodePassword(
            $user,
            '123'
        ));

        $manager->persist($user);
        $manager->flush();
    }
}
