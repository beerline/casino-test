<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 23:21
 */

namespace App\BankOfaAmericaBundle\SerialiseEntity;

class BankOfAmericaPostRequest
{
    /**
     * @var string
     */
    private $userId;

    /**
     * @var float
     */
    private $ammount;

    /**
     * BankOfAmericaPutRequest constructor.
     *
     * @param string $userId
     * @param float  $ammount
     */
    public function __construct( string $userId, float $ammount )
    {
        $this->userId = $userId;
        $this->ammount = $ammount;
    }

    /**
     * @return string
     */
    public function getUserId(): string
    {
        return $this->userId;
    }

    /**
     * @return float
     */
    public function getAmmount(): float
    {
        return $this->ammount;
    }

}