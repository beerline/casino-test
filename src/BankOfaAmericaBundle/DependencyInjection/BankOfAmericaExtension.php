<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 27.11.2018
 * Time: 0:47
 */

namespace App\BankOfaAmericaBundle\DependencyInjection;

use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

class BankOfAmericaExtension extends Extension
{

    /**
     * Loads a specific configuration.
     *
     * @param array            $configs
     * @param ContainerBuilder $container
     */
    public function load( array $configs, ContainerBuilder $container )
    {
        $loader = new  YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');
    }
}