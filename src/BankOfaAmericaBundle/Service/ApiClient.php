<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 27.11.2018
 * Time: 0:57
 */

namespace App\BankOfaAmericaBundle\Service;


use Http\Client\HttpClient;
use Http\Message\RequestFactory;

class ApiClient implements ApiClientInterface
{
    const HTTP_OK = 200;

    private $client;

    /**
     * @var string string
     */
    private $host;

    /**
     * @var RequestFactory
     */
    private $requestFactory;

    /**
     * ApiClient constructor.
     *
     * @param HttpClient     $client
     * @param RequestFactory $requestFactory
     */
    public function __construct( HttpClient $client, RequestFactory $requestFactory)
    {
        $this->client = $client;
        $this->requestFactory = $requestFactory;
    }

    /**
     * Отправляем пост запрос в банк
     *
     * @param string $jsonBody
     *
     * @return bool
     * @throws \Exception
     * @throws \Http\Client\Exception
     */
    public function postMoney( string $jsonBody ): bool
    {
        $apiMethod = '/upload_money';

        $response = $this->send( 'POST', $this->host, $apiMethod, $jsonBody );

        return $response->getStatusCode() === static::HTTP_OK;
    }

    /**
     * @param        $httpMethod
     * @param        $host
     * @param        $apiMethod
     * @param string $bodyJson
     *
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \Exception
     * @throws \Http\Client\Exception
     */
    private function send($httpMethod, $host, $apiMethod, $bodyJson = '' )
    {
        $request = $this->requestFactory->createRequest( $httpMethod,
            $host . '/' . $apiMethod,
            [ 'content-type' => 'application/json' ],
            $bodyJson );
        $response = $this->client->sendRequest($request);
        return $response;
    }
}