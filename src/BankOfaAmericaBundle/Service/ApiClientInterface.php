<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 27.11.2018
 * Time: 1:00
 */

namespace App\BankOfaAmericaBundle\Service;

interface ApiClientInterface
{
    /**
     * Отправляем пост запрос в банк
     *
     * @param string $jsonBody
     *
     * @return bool
     */
    public function postMoney( string $jsonBody) : bool ;
}