<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 17:47
 */

namespace App\Type;

use JaycoDesign\Decimal\Decimal as Calculator;

class Decimal
{
    const SCALE = 0.00001;
    const A_LOWER_B = -1;
    const A_GREATER_B = 1;
    const A_EQUAL_B = 0;

    private $value;

    public function __construct($value)
    {
        $this->value = (string)$value;
    }

    public function __toString()
    {
        return (string)$this->value;
    }

    public function toFloat()
    {
        return (float) $this->value;
    }

    /**
     * Сложение
     *
     * @param $number
     *
     * @return static
     */
    public function add($number)
    {
        return new static(Calculator::add($this->value, (string)$number));
    }

    /**
     * Умножение
     *
     * @param $number
     *
     * @return static
     */
    public function mul($number)
    {
        return new static(Calculator::mul($this->value, (string)$number));
    }

    /**
     * Вычитание
     *
     * @param $number
     *
     * @return static
     */
    public function sub($number)
    {
        return new static(Calculator::sub($this->value, (string)$number));
    }

    /**
     * Деление
     *
     * @param $number
     *
     * @return static
     */
    public function div($number)
    {
        return new static(Calculator::div($this->value, (string)$number));
    }

    /**
     * Сравнение флоатов
     *
     * возвращаемые значени (по аналогии с оператором spaceShip
     * http://php.net/manual/ru/migration70.new-features.php#migration70.new-features.spaceship-op)
     *
     * -1  если $a < $b
     *  0  если $a = $b
     *  1  если $a > $b
     *
     * @param Decimal $operand2
     * @param float $scale точность вычислений
     * @return int
     */
    private function compare(Decimal $operand2, float $scale = self::SCALE): int
    {
        return bccomp($this, $operand2, $scale);
    }

    /**
     * Строгое сравниваем 2-х чисел на "меньше".
     *
     * возвращаемые значени
     * true если $a < $b;
     *
     * @param Decimal $operand2
     * @param float $scale
     *
     * @return bool
     */
    public function lowerThan(Decimal $operand2, float $scale = self::SCALE) : bool
    {
        if ($this->compare($operand2, $scale) === self::A_LOWER_B){
            return true;
        }
        return false;
    }

    /**
     * НЕ строгое сравнение 2-х чисел на "меньше или равно".
     *
     * возвращаемые значени
     * true если $a <= $b;
     *
     * @param Decimal $operand
     * @param float $scale
     *
     * @return bool
     */
    public function lowerThanEqual(Decimal $operand, float $scale = self::SCALE) : bool
    {
        switch ($this->compare($operand, $scale)){
            case self::A_LOWER_B:
            case self::A_EQUAL_B:
                return true;
        }
        return false;
    }

    /**
     * Строгое сравниваем 2-х чисел на "больше" .
     *
     * возвращаемые значени
     * true если $a > $b;
     *
     * @param Decimal $b
     * @param float $scale
     *
     * @return bool
     */
    public function greaterThan(Decimal $b, float $scale = self::SCALE) : bool
    {
        if ($this->compare($b, $scale) === self::A_GREATER_B){
            return true;
        }
        return false;
    }

    /**
     * Строгое сравниваем 2-х чисел на "больше или равно" .
     *
     * возвращаемые значени
     * true если $a >= $b;
     *
     * @param Decimal $b
     * @param float $scale
     *
     * @return bool
     */
    public function greaterThanEqual(Decimal$b, float $scale = self::SCALE) : bool
    {
        switch ($this->compare($b, $scale)){
            case self::A_GREATER_B:
            case self::A_EQUAL_B:
                return true;
        }
        return false;
    }

    /**
     * Проверяем равенство 2-х чисел
     *
     * возвращаемые значени
     * true если $a === $b;
     *
     * @param Decimal $b
     * @param float $scale
     *
     * @return bool
     */
    public function equal(Decimal $b, float $scale = self::SCALE) : bool
    {
        if ($this->compare($b, $scale) === self::A_EQUAL_B){
            return true;
        }
        return false;
    }

}