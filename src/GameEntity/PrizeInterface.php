<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 21:43
 */

namespace App\GameEntity;

interface PrizeInterface
{
    /**
     * Тип приза
     *
     * @return string
     */
    public function getPrizeType() : string;

    public function getPrizeValue() : string;
}