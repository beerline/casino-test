<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 01.12.2018
 * Time: 16:57
 */

namespace App\GameEntity;

use App\Service\PrizeGenerators\Money;
use App\Type\Decimal;

class MoneyPrize extends AbstractPrize
{
    /**
     * @var Decimal
     */
    private $amount;

    /**
     * MoneyPrize constructor.
     *
     * @param Decimal $amount
     */
    public function __construct( Decimal $amount )
    {
        parent::__construct( Money::PRIZE_TYPE_NAME );
        $this->amount = $amount;
    }

    /**
     * @return Decimal
     */
    public function getAmount(): Decimal
    {
        return $this->amount;
    }

    /**
     * Тип приза
     *
     * @return string
     */
    public function getPrizeType(): string
    {
        return Money::PRIZE_TYPE_NAME;
    }

    public function getPrizeValue(): string
    {
        return $this->amount;
    }
}