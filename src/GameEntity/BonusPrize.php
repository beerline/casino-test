<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 22:49
 */

namespace App\GameEntity;

use App\Service\PrizeGenerators\Bonuses;
use App\Type\Decimal;

class BonusPrize extends AbstractPrize
{
    /**
     * сумма начисленных бонусов
     *
     * @var Decimal
     */
    private $amount;

    /**
     * Prize constructor.
     *
     * @param Decimal $amount
     */
    public function __construct( Decimal $amount )
    {
        parent::__construct( Bonuses::PRIZE_TYPE_NAME );

        $this->amount = $amount;
    }

    /**
     * @return Decimal
     */
    public function getAmount() : Decimal
    {
        return $this->amount;
    }

    /**
     * Тип приза
     *
     * @return string
     */
    public function getPrizeType(): string
    {
        return Bonuses::PRIZE_TYPE_NAME;
    }

    public function getPrizeValue(): string
    {
        return $this->amount;
    }
}