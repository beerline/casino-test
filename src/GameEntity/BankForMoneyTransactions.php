<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 22:40
 */

namespace App\GameEntity;

use App\Hdrator\Entity\MoneyTransactionForBanking;

class BankForMoneyTransactions
{
    /**
     * Псевдоним банка
     *
     * @var
     */
    private $bankAlias;

    /**
     * @var MoneyTransactionForBanking[]
     */
    private $moneyTransactionsForBanking;

    /**
     * BankForMoneyForBanking constructor.
     *
     * @param                            $bankAlias
     * @param MoneyTransactionForBanking[] $moneyTransactionsForBanking
     */
    public function __construct( $bankAlias, array $moneyTransactionsForBanking )
    {
        $this->bankAlias = $bankAlias;
        $this->moneyTransactionsForBanking = $moneyTransactionsForBanking;
    }

    /**
     * @return mixed
     */
    public function getBankAlias()
    {
        return $this->bankAlias;
    }

    /**
     * @return MoneyTransactionForBanking[]
     */
    public function getMoneyTransactionsForBanking(): array
    {
        return $this->moneyTransactionsForBanking;
    }


}