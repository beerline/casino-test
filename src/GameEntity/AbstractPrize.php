<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 01.12.2018
 * Time: 16:49
 */

namespace App\GameEntity;

abstract class AbstractPrize implements PrizeInterface
{
    /**
     * @var
     */
    private $prizeType;

    /**
     * Prize constructor.
     *
     * @param $prizeType
     */
    public function __construct( string $prizeType )
    {
        $this->prizeType = $prizeType;
    }
}