<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 01.12.2018
 * Time: 17:00
 */

namespace App\GameEntity;

use App\Service\PrizeGenerators\PhysicalPrize as PhysicalPrizeGenerator;

class PhysicalPrize extends AbstractPrize
{
    /**
     * @var string
     */
    private $prizeName;

    /**
     * PhysicalPrize constructor.
     *
     * @param string $prizeName
     */
    public function __construct( string $prizeName )
    {
        parent::__construct( \App\Service\PrizeGenerators\PhysicalPrize::PRIZE_TYPE_NAME );
        $this->prizeName = $prizeName;
    }

    /**
     * Тип приза
     *
     * @return string
     */
    public function getPrizeType(): string
    {
        return PhysicalPrizeGenerator::PRIZE_TYPE_NAME;
    }

    public function getPrizeValue(): string
    {
        return $this->prizeName;
    }
}