<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 17:32
 */

namespace App\GameEntity\RejectPrize;


use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class Prizes
 *
 * @package App\GameEntity\RejectPrize
 *
 */
class Prize
{
    /** 
     * @var int
     *
     * @Assert\NotNull()
     */
    protected $id;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId( int $id ): void
    {
        $this->id = $id;
    }
}