<?php

namespace App\Repository;

use App\Entity\PhysicalPrizeAvailable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PhysicalPrizeAvailable|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhysicalPrizeAvailable|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhysicalPrizeAvailable[]    findAll()
 * @method PhysicalPrizeAvailable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhysicalPrizeAvailableRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PhysicalPrizeAvailable::class);
    }

    /**
     * TODO реализовать с исопльзованием транзакций. По аналогии с генерацие денег
     *
     * Ищем все доступные к розыгышу призы
     */
    public function findAllAvailablePrizes()
    {
        $qb = $this->createQueryBuilder('ppa');
        return $qb->andWhere('ppa.isAvailableForGame = true')
            ->andWhere('ppa.count > 0')
            ->getQuery()
            ->getResult();
    }

    /**
     * TODO когда будут добавлены транзакции в \App\Repository\PhysicalPrizeAvailableRepository::findAllAvailablePrizes
     * здесь закрывать транзакцию
     *
     * Устанавливаем новое доступное для розыгрыша количество выбранных предметов
     *
     * @param $newAvailableCount
     * @param $id
     */
    public function setNewAvailableCount( $newAvailableCount, $id )
    {
        $this->getEntityManager()
             ->createQueryBuilder()
             ->update(PhysicalPrizeAvailable::class, 'ppa')
             ->set('ppa.count', ":newAvailableCount")
             ->where("ppa.id = :id")
             ->setParameter(':newAvailableCount', $newAvailableCount)
             ->setParameter(':id', $id)
             ->getQuery()
             ->execute()
        ;
    }

    /**
     * Возвращаем один приз вдоступные для розыгрыша
     *
     * @param PhysicalPrizeAvailable $physicalPrizeAvailable
     */
    public function restorePrizeCountByOne( PhysicalPrizeAvailable $physicalPrizeAvailable )
    {
        $this->getEntityManager()
             ->createQueryBuilder()
            ->update(PhysicalPrizeAvailable::class, 'ppa')
            ->set('ppa.count', $physicalPrizeAvailable->getCount() + 1)
            ->where('ppa.id = :id')
            ->setParameter(':id', $physicalPrizeAvailable->getId())
            ->getQuery()
            ->execute();
    }
}
