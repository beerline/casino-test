<?php

namespace App\Repository;

use App\Entity\MoneyAvailable;
use App\Type\Decimal;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MoneyAvailable|null find($id, $lockMode = null, $lockVersion = null)
 * @method MoneyAvailable|null findOneBy(array $criteria, array $orderBy = null)
 * @method MoneyAvailable[]    findAll()
 * @method MoneyAvailable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MoneyAvailableRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, MoneyAvailable::class);
    }

    /**
     * TODO тут надо завернуть в транзакцию что бы исключить случай обновления доступных бонусов
     * до обновления суммы текущей операцией
     *
     * Возвращаем доступные для розыгрыша деньги и диапозон розыгрыша
     *
     * @return MoneyAvailable|null
     *
     */
    public function findAvailableMoney() : ?MoneyAvailable
    {
        $result = $this->findOneBy([]);

        return $result;
    }

    /**
     * TODO когда будут добавлены транзакции в \App\Repository\MoneyAvailableRepository::findAvailableMoney
     * здесь закрывать транзакцию
     *
     * Устанавливаем новое доступное для розыгрыша количество денег
     *
     * @param $newAvailableSum
     * @param $id
     */
    public function setNewAvailableSum( Decimal $newAvailableSum, $id )
    {
        $this->getEntityManager()
            ->createQueryBuilder()
            ->update(MoneyAvailable::class, 'money_available')
                ->set('money_available.sum', ":newAvailableSum")
            ->where("money_available.id = :id")
            ->setParameter(':newAvailableSum', $newAvailableSum)
            ->setParameter(':id', $id)
            ->getQuery()
            ->execute()
        ;
    }
}
