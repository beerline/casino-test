<?php

namespace App\Repository\Transactions;

use App\Entity\BonusTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BonusTransaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method BonusTransaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method BonusTransaction[]    findAll()
 * @method BonusTransaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BonusTransactionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BonusTransaction::class);
    }
}
