<?php

namespace App\Repository\Transactions;

use App\Entity\PhysicalPrizeTransaction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method PhysicalPrizeTransaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method PhysicalPrizeTransaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method PhysicalPrizeTransaction[]    findAll()
 * @method PhysicalPrizeTransaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PhysicalPrizeTransactionRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, PhysicalPrizeTransaction::class);
    }
}
