<?php

namespace App\Repository\Transactions;

use App\Entity\MoneyTransaction;
use App\Hdrator\AbstractMoneyTransactionForBankingHydrator;
use App\Hdrator\Entity\MoneyTransactionForBanking;
use App\Hdrator\MoneyTransactionForBankingHydratorInterface;
use App\Hdrator\PgSQL\MoneyTransactionForBankingHydrator;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method MoneyTransaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method MoneyTransaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method MoneyTransaction[]    findAll()
 * @method MoneyTransaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class MoneyTransactionRepository extends ServiceEntityRepository
{
    /**
     * @var MoneyTransactionForBankingHydratorInterface
     */
    private $bankingHydrator;

    public function __construct(RegistryInterface $registry, MoneyTransactionForBankingHydratorInterface $bankingHydrator)
    {
        parent::__construct($registry, MoneyTransaction::class);
        $this->bankingHydrator = $bankingHydrator;
    }

    /**
     * Подсчитываем суммы которы нужно отправить в банки
     *
     * @return MoneyTransactionForBanking[]|null
     */
    public function findMoneyForBanking() : ?array
    {
        $query = $this->createQueryBuilder('mt')
            ->select( 'IDENTITY(mt.user) as ' . AbstractMoneyTransactionForBankingHydrator::HYDRATE_FIELD_NAME_USER
                , 'SUM(mt.amount) as ' . AbstractMoneyTransactionForBankingHydrator::HYDRATE_FIELD_NAME_AMOUNT)
            ->groupBy('mt.user')
            ->having( 'SUM(mt.amount) > 0' )
            ->getQuery();

        $result = $query->execute();

        if ( count( $result ) === 0 ) {
            return null;
        }

        return $this->bankingHydrator->hydrateArrayObjects( $result );
    }


}
