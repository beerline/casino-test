<?php

namespace App\Repository;

use App\Entity\BonusesAvailable;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method BonusesAvailable|null find($id, $lockMode = null, $lockVersion = null)
 * @method BonusesAvailable|null findOneBy(array $criteria, array $orderBy = null)
 * @method BonusesAvailable[]    findAll()
 * @method BonusesAvailable[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BonusesAvailableRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, BonusesAvailable::class);
    }

    /**
     * Ищем диапозон генерации бонусных баллов
     *
     * @return BonusesAvailable|null
     */
    public function findRange() : ?BonusesAvailable
    {
        return $this->findOneBy([]);
    }

}
