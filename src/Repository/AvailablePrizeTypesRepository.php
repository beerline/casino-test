<?php

namespace App\Repository;

use App\Entity\AvailablePrizeTypes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Symfony\Bridge\Doctrine\RegistryInterface;

/**
 * @method AvailablePrizeTypes|null find($id, $lockMode = null, $lockVersion = null)
 * @method AvailablePrizeTypes|null findOneBy(array $criteria, array $orderBy = null)
 * @method AvailablePrizeTypes[]    findAll()
 * @method AvailablePrizeTypes[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AvailablePrizeTypesRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, AvailablePrizeTypes::class);
    }

    /**
     * Ищем доступные для разыгрыша типы призов
     */
    public function findAvailablePrizeType()
    {
        $qb = $this->createQueryBuilder('apt');
        return $qb->andWhere('apt.enable = true')
            ->getQuery()
            ->getResult();

    }

}
