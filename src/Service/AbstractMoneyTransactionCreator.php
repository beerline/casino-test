<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 27.11.2018
 * Time: 0:07
 */

namespace App\Service;

use Doctrine\Common\Persistence\ObjectManager;

abstract class AbstractMoneyTransactionCreator implements MoneyTransactionCreatorInterface
{
    /**
     * @var ObjectManager
     */
    protected $objectManager;

    /**
     * AbstractMoneyTransactionCreator constructor.
     *
     * @param ObjectManager $objectManager
     */
    public function __construct( ObjectManager $objectManager )
    {
        $this->objectManager = $objectManager;
    }

}