<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 12:17
 */

namespace App\Service\PrizeGenerators\Randomizer\Money;

use App\Type\Decimal;

interface MoneyRandomizerInterface
{
    /**
     * Гененрируем рандомные бонусы в заданных диапозонах
     *
     * @param Decimal $min
     * @param Decimal $max
     *
     * @return Decimal
     */
    public function random( Decimal $min, Decimal $max ) : Decimal;
}