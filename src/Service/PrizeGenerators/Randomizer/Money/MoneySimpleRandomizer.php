<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 12:17
 */

namespace App\Service\PrizeGenerators\Randomizer\Money;

use App\Type\Decimal;

class MoneySimpleRandomizer implements MoneyRandomizerInterface
{

    /**
     * Гененрируем рандомную денежную сумму в заданных диапозонах
     *
     * @param Decimal $min
     * @param Decimal $max
     *
     * @return Decimal
     */
    public function random( Decimal $min, Decimal $max ): Decimal
    {
        return (new Decimal( rand( $min->toFloat(), $max->toFloat() ) ) );
    }
}