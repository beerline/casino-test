<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 15:09
 */

namespace App\Service\PrizeGenerators\Randomizer\PhysicalPrize;

use App\Entity\PhysicalPrizeAvailable;

interface PhysicalPrizeRandomizerInterface
{
    /**
     * Выбираем случайный приз
     *
     * @return PhysicalPrizeAvailable|null
     */
    public function findRandomPrize() : ? PhysicalPrizeAvailable ;
}