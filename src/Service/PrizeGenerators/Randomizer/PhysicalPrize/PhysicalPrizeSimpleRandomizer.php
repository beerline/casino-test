<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 15:16
 */

namespace App\Service\PrizeGenerators\Randomizer\PhysicalPrize;

use App\Entity\PhysicalPrizeAvailable;
use App\Repository\PhysicalPrizeAvailableRepository;

class PhysicalPrizeSimpleRandomizer implements PhysicalPrizeRandomizerInterface
{
    /**
     * @var PhysicalPrizeAvailableRepository
     */
    private $physicalPrizeAvailableRepository;

    /**
     * PhysicalPrizeSimpleRandomizer constructor.
     *
     * @param PhysicalPrizeAvailableRepository $physicalPrizeAvailableRepository
     */
    public function __construct( PhysicalPrizeAvailableRepository $physicalPrizeAvailableRepository )
    {
        $this->physicalPrizeAvailableRepository = $physicalPrizeAvailableRepository;
    }

    /**
     * Выбираем случайный приз
     *
     * @return PhysicalPrizeAvailable|null
     */
    public function findRandomPrize(): ? PhysicalPrizeAvailable
    {
        $prizes = $this->physicalPrizeAvailableRepository->findAllAvailablePrizes();

        if ( count($prizes) <= 0 ) {
            return null;
        }

        shuffle($prizes);

        return array_shift( $prizes );
    }
}