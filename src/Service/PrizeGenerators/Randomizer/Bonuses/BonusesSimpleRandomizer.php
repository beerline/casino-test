<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 0:53
 */

namespace App\Service\PrizeGenerators\Randomizer\Bonuses;

use App\Type\Decimal;

class BonusesSimpleRandomizer implements BonusesRandomizerInterface
{
    /**
     * Гененрируем рандомные бонусы в заданных диапозонах
     *
     * @param Decimal $min
     * @param Decimal $max
     *
     * @return Decimal
     */
    public function random( Decimal $min, Decimal $max ): Decimal
    {

        return (new Decimal( rand( $min->toFloat(), $max->toFloat() ) ) );
    }
}