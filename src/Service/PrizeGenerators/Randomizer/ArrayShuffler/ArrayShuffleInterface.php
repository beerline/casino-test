<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 21:40
 */

namespace App\Service\PrizeGenerators\Randomizer\ArrayShuffler;

interface ArrayShuffleInterface
{
    /**
     * Перемешиваем массив
     *
     * @param array $array
     *
     * @return array
     */
    public function shuffle(array $array) : array ;
}