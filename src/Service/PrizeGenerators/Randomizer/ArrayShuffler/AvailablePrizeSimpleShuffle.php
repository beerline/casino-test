<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 21:39
 */

namespace App\Service\PrizeGenerators\Randomizer\ArrayShuffler;

use App\Service\PrizeGenerators\Bonuses;
use App\Service\PrizeGenerators\Money;
use App\Service\PrizeGenerators\PhysicalPrize;

class AvailablePrizeSimpleShuffle implements ArrayShuffleInterface
{

    /**
     * Перемешиваем массив
     *
     * @param array $array
     *
     * @return array
     */
    public function shuffle(array $array): array
    {
        shuffle($array);

        return $array;
    }
}