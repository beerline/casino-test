<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 21:29
 */

namespace App\Service\PrizeGenerators;


use App\Entity\MoneyAvailable;
use App\Entity\MoneyTransaction;
use App\Exception\MoneyAvailableNotFoundException;
use App\GameEntity\MoneyPrize;
use App\GameEntity\PrizeInterface;
use App\Repository\MoneyAvailableRepository;
use App\Service\PrizeGenerators\Randomizer\Money\MoneyRandomizerInterface;
use App\Type\Decimal;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class Money extends AbstractPrizeGenerator
{
    const PRIZE_TYPE_NAME = 'money';

    /**
     * @var MoneyRandomizerInterface
     */
    private $randomizer;

    /**
     * @var MoneyAvailableRepository
     */
    private $moneyAvailableRepository;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * Money constructor.
     *
     * @param MoneyRandomizerInterface $randomizer
     * @param MoneyAvailableRepository $repository
     * @param Security                 $security
     * @param ObjectManager            $objectManager
     */
    public function __construct(
        MoneyRandomizerInterface $randomizer,
        MoneyAvailableRepository $repository,
        Security $security,
        ObjectManager $objectManager
    ) {
        $this->randomizer = $randomizer;
        $this->moneyAvailableRepository = $repository;
        $this->user = $security->getUser();
        $this->objectManager = $objectManager;
    }

    /**
     * Метода возвращает названиае генератора для сопоставляния с данным в БД
     *
     * @return mixed
     */
    public static  function getGeneratorTypeName(): string
    {
        return static::PRIZE_TYPE_NAME;
    }

    /**
     * @param $availablePrizeType
     *
     * @return bool
     */
    protected function canGenerate( $availablePrizeType )
    {
        return $availablePrizeType === static::PRIZE_TYPE_NAME;
    }

    /**
     * Случайныйм образом выбираем приз
     *
     * @param string $availablePrizeType
     *
     * @return PrizeInterface|null
     *
     * @throws MoneyAvailableNotFoundException
     * @throws \Doctrine\ORM\TransactionRequiredException
     */
    public function generate( string $availablePrizeType ): ?PrizeInterface
    {
        if ( ! $this->canGenerate( $availablePrizeType ) ) {
            return parent::generate( $availablePrizeType );
        }

        // Генерируем призовую сумму
        $moneyAvailable = $this->moneyAvailableRepository->findAvailableMoney();

        if ( ! $moneyAvailable instanceof  MoneyAvailable) {
            return null;
        }

        $amountGenerated = new Decimal($this->randomizer
            ->random( $moneyAvailable->getMinGenerateRange(), $moneyAvailable->getMaxGenerateRange() )
        );

        /**
         * Если количество сумма сгенерированного приза больше доступной то возвращаем null что бы сгенерировыть
         * другие призы
         */
        if ( $amountGenerated > $moneyAvailable->getSum()
            || $amountGenerated > $moneyAvailable->getMaxGenerateRange()
            || $amountGenerated < $moneyAvailable->getMinGenerateRange()
        ) {
            return null;
        }

        // списание со счета лимита
        $this->moneyAvailableRepository->setNewAvailableSum(
            $moneyAvailable->getSum()->sub( $amountGenerated )
            , $moneyAvailable->getId()
        );

        // запсь в транзакции
        $transaction = new MoneyTransaction(MoneyTransaction::TYPE_ACCRUAL, $amountGenerated, $this->user);
        $this->objectManager->persist($transaction);

        $this->objectManager->flush();

        return new MoneyPrize( $amountGenerated );

    }

    /**
     * @return UserInterface
     */
    public function getUser() : UserInterface
    {
        return $this->user;
    }
}