<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 22:43
 */

namespace App\Service\PrizeGenerators\ChainBuilder;

use App\Service\PrizeGenerators\Bonuses;
use App\Service\PrizeGenerators\Money;
use App\Service\PrizeGenerators\PhysicalPrize;
use App\Service\PrizeGenerators\PrizeGeneratorInterface;

class ChainBuilder implements ChainBuilderInterface
{
    /**
     * @var Bonuses
     */
    private $bonusesGenerator;

    /**
     * @var Money
     */
    private $moneyGenerator;

    /**
     * @var PhysicalPrize
     */
    private $physicalPrizeGenerator;

    /**
     * ChainBuilder constructor.
     *
     * @param Bonuses $bonusesGenerator
     * @param Money $moneyGenerator
     * @param PhysicalPrize $physicalPrizeGenerator
     */
    public function __construct(
        Bonuses $bonusesGenerator,
        Money $moneyGenerator,
        PhysicalPrize $physicalPrizeGenerator )
    {
        $this->bonusesGenerator = $bonusesGenerator;
        $this->moneyGenerator = $moneyGenerator;
        $this->physicalPrizeGenerator = $physicalPrizeGenerator;
    }

    /**
     * Строим цепочку из генераторов которые могут сгенерировать приз
     *
     * @return PrizeGeneratorInterface
     */
    public function build(): PrizeGeneratorInterface
    {
        $this->bonusesGenerator->setNext($this->moneyGenerator)->setNext($this->physicalPrizeGenerator);

        return $this->bonusesGenerator;
    }
}