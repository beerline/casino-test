<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 22:42
 */

namespace App\Service\PrizeGenerators\ChainBuilder;

use App\Service\PrizeGenerators\PrizeGeneratorInterface;

interface ChainBuilderInterface
{
    /**
     * Строим цепочку из генераторов которые могут сгенерировать приз
     *
     * @return PrizeGeneratorInterface
     */
    public function build() : PrizeGeneratorInterface;
}