<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 22:45
 */

namespace App\Service\PrizeGenerators;

use App\GameEntity\PrizeInterface;

abstract class AbstractPrizeGenerator implements PrizeGeneratorInterface
{
    /**
     * @var PrizeGeneratorInterface
     */
    private $nextGenerator;

    public function generate( string $availablePrizeType ): ?PrizeInterface
    {
        if ( $this->nextGenerator instanceof PrizeGeneratorInterface) {
            $prize = $this->nextGenerator->generate($availablePrizeType);
        } else {
            $prize = null;
        }

        return $prize;
    }

    /**
     * @param PrizeGeneratorInterface $generator
     *
     * @return PrizeGeneratorInterface
     */
    public function setNext( PrizeGeneratorInterface $generator ): PrizeGeneratorInterface
    {
        $this->nextGenerator = $generator;

        return $generator;
    }


}