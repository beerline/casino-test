<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 21:30
 */

namespace App\Service\PrizeGenerators;


use App\Entity\PhysicalPrizeAvailable;
use App\Entity\PhysicalPrizeTransaction;
use App\Entity\User;
use App\Exception\AvailablePhysicalPrizeNotFoundException;
use App\GameEntity\PrizeInterface;
use App\Repository\PhysicalPrizeAvailableRepository;
use App\Service\PrizeGenerators\Randomizer\PhysicalPrize\PhysicalPrizeRandomizerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Security;
use App\GameEntity;

class PhysicalPrize extends AbstractPrizeGenerator
{
    const PRIZE_TYPE_NAME = 'physical_prize';

    /**
     * @var PhysicalPrizeRandomizerInterface
     */
    private $randomizer;

    /**
     * @var PhysicalPrizeAvailableRepository
     */
    private $physicalPrizeAvailableRepository;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var User
     */
    private $user;

    /**
     * PhysicalPrize constructor.
     *
     * @param PhysicalPrizeRandomizerInterface $randomizer
     * @param PhysicalPrizeAvailableRepository $physicalPrizeAvailableRepository
     * @param ObjectManager                    $objectManajer
     * @param Security                         $security
     */
    public function __construct(
        PhysicalPrizeRandomizerInterface $randomizer,
        PhysicalPrizeAvailableRepository $physicalPrizeAvailableRepository,
        ObjectManager $objectManajer,
        Security $security)
    {
        $this->randomizer = $randomizer;
        $this->physicalPrizeAvailableRepository = $physicalPrizeAvailableRepository;
        $this->objectManager = $objectManajer;
        $this->user = $security->getUser();
    }

    /**
     * Метода возвращает названиае генератора для сопоставляния с данным в БД
     *
     * @return mixed
     */
    public static function getGeneratorTypeName(): string
    {
        return static::PRIZE_TYPE_NAME;
    }

    /**
     * @param $availablePrizeType
     *
     * @return bool
     */
    protected function canGenerate( $availablePrizeType )
    {
        return $availablePrizeType === static::PRIZE_TYPE_NAME;
    }

    /**
     * Случайныйм образом выбираем приз
     *
     * @param string $availablePrizeType
     *
     * @return PrizeInterface|null
     * @throws AvailablePhysicalPrizeNotFoundException
     */
    public function generate( string $availablePrizeType ): ?PrizeInterface
    {
        if ( ! $this->canGenerate( $availablePrizeType ) ) {
            return parent::generate( $availablePrizeType );
        }

        // Выбираем произвольный подарок
        $prise = $this->randomizer->findRandomPrize();

        if ( ! $prise instanceof PhysicalPrizeAvailable ) {
            return null;
        }

        // уменьшаем количество доступных для розыгрыша призов на 1
        $this->physicalPrizeAvailableRepository->setNewAvailableCount(
            $prise->getCount() - 1
            , $prise->getId()
        );

        // запсь в транзакции
        $physicalPrizeTransaction = new PhysicalPrizeTransaction(
          PhysicalPrizeTransaction::STATUS_ACCRUAL
          , $this->user
          , $prise
        );
        $this->objectManager->persist($physicalPrizeTransaction);
        $this->objectManager->flush();

        return new GameEntity\PhysicalPrize( $prise->getName() );
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }
}