<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 21:19
 */

namespace App\Service\PrizeGenerators;


use App\GameEntity\PrizeInterface;

interface PrizeGeneratorInterface
{
    /**
     * Случайныйм образом выбираем приз
     *
     * @param string $availablePrizeType
     *
     * @return mixed
     */
    public function generate( string $availablePrizeType ) : ?PrizeInterface;

    /**
     * Метода возвращает названиае генератора для сопоставляния с данным в БД
     *
     * @return mixed
     */
    public static function getGeneratorTypeName() : string ;

    /**
     * Устанавливаем следубщий элемент цепочки
     *
     * @param PrizeGeneratorInterface $generator
     *
     * @return PrizeGeneratorInterface
     */
    public function setNext( PrizeGeneratorInterface $generator) : PrizeGeneratorInterface;

}