<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 21:30
 */

namespace App\Service\PrizeGenerators;


use App\Entity\BonusesAvailable;
use App\Entity\BonusTransaction;
use App\Exception\BonusRangeNotFoundException;
use App\GameEntity\BonusPrize;
use App\GameEntity\PrizeInterface;
use App\Repository\BonusesAvailableRepository;
use App\Service\PrizeGenerators\Randomizer\Bonuses\BonusesRandomizerInterface;
use App\Type\Decimal;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Security\Core\User\UserInterface;

class Bonuses extends AbstractPrizeGenerator
{
    const PRIZE_TYPE_NAME = 'bonuses';

    /**
     * @var BonusesRandomizerInterface
     */
    private $randomizer;

    /**
     * @var BonusesAvailableRepository
     */
    private $bonusesAvailableRepositorey;

    /**
     * @var UserInterface
     */
    private $user;

    /**
     * @var ObjectManager
     */
    private $objectManage;

    /**
     * Bonuses constructor.
     *
     * @param BonusesRandomizerInterface $randomizer
     * @param BonusesAvailableRepository $repository
     * @param Security                   $security
     * @param ObjectManager              $objectManager
     */
    public function __construct(
        BonusesRandomizerInterface $randomizer,
        BonusesAvailableRepository $repository,
        Security $security,
        ObjectManager $objectManager
    ) {
        $this->randomizer = $randomizer;
        $this->bonusesAvailableRepositorey = $repository;
        $this->user = $security->getUser();
        $this->objectManage = $objectManager;
    }

    /**
     * Метода возвращает названиае генератора для сопоставляния с данным в БД
     *
     * @return mixed
     */
    public static function getGeneratorTypeName(): string
    {
        return static::PRIZE_TYPE_NAME;
    }

    /**
     * @param $availablePrizeType
     *
     * @return bool
     */
    protected function canGenerate( $availablePrizeType )
    {
        return $availablePrizeType === static::PRIZE_TYPE_NAME;
    }

    /**
     * Случайныйм образом выбираем приз
     *
     * @param string $availablePrizeType
     *
     * @return PrizeInterface|null
     * @throws BonusRangeNotFoundException
     */
    public function generate( string $availablePrizeType ): ?PrizeInterface
    {
        if ( ! $this->canGenerate( $availablePrizeType ) ) {
            return parent::generate( $availablePrizeType );
        }

        $range = $this->bonusesAvailableRepositorey->findRange();

        if ( !$range instanceof BonusesAvailable) {
            return null;
        }

        $bonuses = $this->randomizer->random( $range->getMinGenerateRange(), $range->getMaxGenerateRange());

        if ($bonuses > $range->getMaxGenerateRange()
            || $bonuses < $range->getMinGenerateRange()
        ) {
            return null;
        }

        $this->creteTransaction( $bonuses );

        return new BonusPrize( $bonuses );
    }

    /**
     * Создаём и сохраняем транзакцию начисления бонусов
     *
     * @param Decimal $bonuses
     */
    private function creteTransaction( Decimal $bonuses )
    {
        $transaction = new BonusTransaction( BonusTransaction::TYPE_ACCRUAL, $bonuses, $this->user);

        $this->objectManage->persist($transaction);

        $this->objectManage->flush();
    }

    /**
     * @return UserInterface
     */
    public function getUser(): UserInterface
    {
        return $this->user;
    }
}