<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 24.11.2018
 * Time: 21:15
 */

namespace App\Service;


use App\Entity\AvailablePrizeTypes;
use App\GameEntity\BonusPrize;
use App\GameEntity\PrizeInterface;
use App\Repository\AvailablePrizeTypesRepository;
use App\SerialiseEntity\GeneratePrizeResponse;
use App\Service\PrizeGenerators\Bonuses;
use App\Service\PrizeGenerators\ChainBuilder\ChainBuilderInterface;
use App\Service\PrizeGenerators\Randomizer\ArrayShuffler\ArrayShuffleInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\SerializerInterface;

class PrizeGenerator implements BusinessLogicServiceInterface
{
    /**
     * @var AvailablePrizeTypesRepository
     */
    private $availablePrizeTypesRepository;

    /**
     * @var ArrayShuffleInterface
     */
    private $arrayShuffler;

    /**
     * @var ChainBuilderInterface
     */
    private $chainBuilder;

    /**
     * @var Bonuses
     */
    private $bonusesGenerator;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * PrizeGenerator constructor.
     *
     * @param AvailablePrizeTypesRepository $availablePrizeTypesRepository
     * @param ArrayShuffleInterface         $arrayShuffle
     * @param ChainBuilderInterface         $chainBuilder
     * @param Bonuses                       $bonusesGenerator
     * @param SerializerInterface           $serializer
     */
    public function __construct(
        AvailablePrizeTypesRepository $availablePrizeTypesRepository,
        ArrayShuffleInterface $arrayShuffle,
        ChainBuilderInterface $chainBuilder,
        Bonuses $bonusesGenerator,
        SerializerInterface $serializer
    ) {
        $this->availablePrizeTypesRepository = $availablePrizeTypesRepository;
        $this->arrayShuffler = $arrayShuffle;
        $this->chainBuilder = $chainBuilder;
        $this->bonusesGenerator = $bonusesGenerator;
        $this->serializer = $serializer;
    }

    /**
     * Выбираем приз случайным способом
     *
     * @return Response
     *
     * @throws \App\Exception\BonusRangeNotFoundException
     */
    public function handle() : Response
    {
        $result = $this->generate();

        return $this->asHttpResponseWithJsonBody( $result );
    }

    private function asHttpResponseWithJsonBody( PrizeInterface $prize) : Response
    {
        $responsePrize = new GeneratePrizeResponse(
            $prize->getPrizeType(),
            $prize->getPrizeValue()
        );

        $responseJson = $this->serializer->serialize( $responsePrize, 'json' );

        $response = new JsonResponse(
            $responseJson,
            Response::HTTP_OK,
            [],
            true
        );

        return $response;
    }

    /**
     * @return PrizeInterface
     * @throws \App\Exception\BonusRangeNotFoundException
     */
    public function generate() : PrizeInterface
    {
        /**
         * Получаем массив доступных для разыгрывания призов
         */
        $availablePrizes = $this->getAvailablePrizeTypes();

        /**
         * Перемешиваем массив
         */
        $mixedAvailablePrizes = $this->arrayShuffler->shuffle($availablePrizes);

        /**
         * Формируем Цепочку
         */
        $chain = $this->chainBuilder->build();

        $prize = null;
        /**
         * Заускаем цикл генерации призов по массиву.
         *
         * Если удалось сгенерировать приз то выходим из цикла/
         * Если приз не сгенерирован (нет достаточного количества) то переходим в следующему типу
         */
        foreach ($mixedAvailablePrizes as $availablePrize) {

            if ( ! $availablePrize instanceof AvailablePrizeTypes ) {
                continue;
            }

            $prize = $chain->generate( $availablePrize->getName() );

            if ( $prize instanceof BonusPrize) {
                break;
            }
        }

        /**
         * Если не удалось сгенерировать ни один приз то генерируем бонусные баллы.
         * Это страховочный вариант, если по каой то причиние генератор бонусов отсутствует в таблице available_prize_type
         */
        if ( $prize === null) {
            $this->bonusesGenerator->generate($this->bonusesGenerator::PRIZE_TYPE_NAME);
        }

        return $prize;
    }

    /**
     * @return array
     */
    private function getAvailablePrizeTypes() : array
    {
        $res = $this->availablePrizeTypesRepository->findAvailablePrizeType();

        return $res;
    }
}