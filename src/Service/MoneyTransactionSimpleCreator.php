<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 27.11.2018
 * Time: 0:08
 */

namespace App\Service;

use App\Entity\MoneyTransaction;
use App\Entity\User;
use App\Type\Decimal;

class MoneyTransactionSimpleCreator extends AbstractMoneyTransactionCreator
{

    public function createMoveToBankTransaction( User $user, Decimal $amount )
    {
        $negativeAmount = new Decimal( -$amount->toFloat() );
        $transaction = new MoneyTransaction(
            MoneyTransaction::TYPE_MOVED_TO_BANK_ACCOUNT
            , $negativeAmount
            , $user
        );

        $this->objectManager->persist( $transaction );
        $this->objectManager->flush();
    }
}