<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 17:22
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\Request;

interface RequestRequireInterface
{
    /**
     * Добавляем объект реквест в сервис
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function setRequest( Request $request);
}