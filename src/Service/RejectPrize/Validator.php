<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 18:12
 */

namespace App\Service\RejectPrize;

use App\Exception\InvalidParamsRejectPrizeException;
use App\GameEntity\RejectPrize\Prize;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class Validator implements RejectPrizeValidatorInterface
{
    /**
     * @var ValidatorInterface
     */
    private $validator;

    /**
     * Validator constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct( ValidatorInterface $validator )
    {
        $this->validator = $validator;
    }

    /**
     * Валидируем тело запроса
     *
     * @param Prize $prize
     *
     * @return bool
     *
     * @throws InvalidParamsRejectPrizeException
     */
    public function validate( Prize $prize ): bool
    {
        $errors = $this->validator->validate($prize);

        if ( count($errors) > 0 ) {
            throw new InvalidParamsRejectPrizeException( Response::HTTP_BAD_REQUEST, (string) $errors);
        }

        return true;
    }
}