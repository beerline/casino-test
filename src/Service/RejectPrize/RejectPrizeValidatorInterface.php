<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 18:10
 */

namespace App\Service\RejectPrize;

use App\GameEntity\RejectPrize\Prize;

interface RejectPrizeValidatorInterface
{
    /**
     * Валидируем тело запроса
     *
     * @param Prize $prize
     *
     * @return bool
     */
    public function validate( Prize $prize ) : bool;
}