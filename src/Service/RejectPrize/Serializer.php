<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 17:29
 */

namespace App\Service\RejectPrize;

use App\GameEntity\RejectPrize\Prize;
use Symfony\Component\Serializer\SerializerInterface;

class Serializer implements RejectPrizeSerializeInterface
{
    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @param SerializerInterface $serializer
     */
    public function __construct( SerializerInterface $serializer )
    {
        $this->serializer = $serializer;
    }

    /**
     * Сериализуем тело запросв
     *
     * @param string $requestContent
     *
     * @return Prize
     */
    public function serialize( string $requestContent ) : Prize
    {
        return $this->serializer->deserialize($requestContent, Prize::class, 'json');
    }
}