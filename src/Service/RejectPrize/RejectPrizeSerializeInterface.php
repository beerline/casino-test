<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 17:30
 */

namespace App\Service\RejectPrize;

use App\GameEntity\RejectPrize\Prize;

interface RejectPrizeSerializeInterface
{
    /**
     * Сериализуем тело запросв
     *
     * @param string $requestContent
     *
     * @return Prize
     */
    public function serialize( string $requestContent ) : Prize;
}