<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 27.11.2018
 * Time: 0:05
 */

namespace App\Service;

use App\Entity\User;
use App\Type\Decimal;

interface MoneyTransactionCreatorInterface
{
    public function createMoveToBankTransaction( User $user, Decimal $amount );
}