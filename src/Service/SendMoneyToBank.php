<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 23:52
 */

namespace App\Service;

use App\GameEntity\BankForMoneyTransactions;
use App\Hdrator\Entity\MoneyTransactionForBanking;
use App\Repository\Transactions\MoneyTransactionRepository;
use App\Service\Banking\ChainBuilder\BankingChainBuilderInterface;

class SendMoneyToBank implements MoneyToBankServiceInterface
{
    /**
     * @var MoneyTransactionRepository
     */
    private $moneyTransactionRepository;

    /**
     * @var BankingChainBuilderInterface
     */
    private $bankingChainBuilder;

    /**
     * SendMoneyToBank constructor.
     *
     * @param MoneyTransactionRepository   $moneyTransactionRepository
     * @param BankingChainBuilderInterface $bankingChainBuilder
     */
    public function __construct(
        MoneyTransactionRepository $moneyTransactionRepository,
        BankingChainBuilderInterface $bankingChainBuilder
    ) {
        $this->moneyTransactionRepository = $moneyTransactionRepository;
        $this->bankingChainBuilder = $bankingChainBuilder;
    }

    /**
     * Ище доступные для отправки в банк деньги и отправяляем их
     */
    public function findAndSend()
    {
        // проходим по таблице с транзакциями денег, суммируем транзакции и группируем по пользователям
        $moneyForBanking = $this->moneyTransactionRepository->findMoneyForBanking();

        // выбранных пользовталей группирум по банкам
        $banksForMoneyTransactions = $this->groupByUserBank($moneyForBanking);

        $bankingServices = $this->bankingChainBuilder->build();
        // по выбранным банкам запускаем цикл
        foreach ($banksForMoneyTransactions as $bank) {
            $bankingServices->send( $bank );
        }
    }

    /**
     * Группируем по пользовательским банкам
     *
     * @param array $moneyForBanking
     *
     * @return BankForMoneyTransactions[]
     */
    private function groupByUserBank( array $moneyForBanking) : array
    {
        $bankForUsers = [];

        /** @var MoneyTransactionForBanking $element */
        foreach ($moneyForBanking as $element) {

            // группируем пользователей по банкам
            $bankForUsers[ $element->getUser()->getBank() ] [] = $element;
        }

        $bankForMoneyTransactions = [];
        foreach ($bankForUsers as $bankAlias => $usersAndMoney) {

            $bankForMoneyTransactions[] = new BankForMoneyTransactions( $bankAlias, $usersAndMoney );
        }

        return $bankForMoneyTransactions;
    }
}