<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 1:14
 */

namespace App\Service;

use Symfony\Component\HttpFoundation\Response;

interface BusinessLogicServiceInterface
{
    public function handle() : Response;
}