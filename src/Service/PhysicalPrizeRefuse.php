<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 25.11.2018
 * Time: 16:26
 */

namespace App\Service;

use App\Entity\PhysicalPrizeTransaction;
use App\Entity\User;
use App\Exception\RejectedPhysicalPrizeTransactionNotFoundException;
use App\Repository\PhysicalPrizeAvailableRepository;
use App\Repository\Transactions\PhysicalPrizeTransactionRepository;
use App\Service\RejectPrize\RejectPrizeSerializeInterface;
use App\Service\RejectPrize\RejectPrizeValidatorInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Security;

class PhysicalPrizeRefuse implements BusinessLogicServiceInterface, RequestRequireInterface
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var Request
     */
    private $request;

    /**
     * @var RejectPrizeSerializeInterface
     */
    private $serializer;

    /**
     * @var RejectPrizeValidatorInterface
     */
    private $validator;

    /**
     * @var PhysicalPrizeTransactionRepository
     */
    private $transactionRepository;

    /**
     * @var ObjectManager
     */
    private $objectManager;

    /**
     * @var PhysicalPrizeAvailableRepository
     */
    private $prizeAvailableRepository;

    /**
     * PhysicalPrizeRefuse constructor.
     *
     * @param Security                           $security
     * @param RejectPrizeSerializeInterface      $serializer
     * @param RejectPrizeValidatorInterface      $validator
     * @param PhysicalPrizeTransactionRepository $transactionRepository
     * @param ObjectManager                      $objectManager
     */
    public function __construct(
        Security $security,
        RejectPrizeSerializeInterface $serializer,
        RejectPrizeValidatorInterface $validator,
        PhysicalPrizeTransactionRepository $transactionRepository,
        ObjectManager $objectManager,
        PhysicalPrizeAvailableRepository $prizeAvailableRepository
    ) {
        $this->user = $security->getUser();
        $this->serializer = $serializer;
        $this->validator = $validator;
        $this->transactionRepository = $transactionRepository;
        $this->objectManager = $objectManager;
        $this->prizeAvailableRepository = $prizeAvailableRepository;
    }

    /**
     * @return Response
     * @throws RejectedPhysicalPrizeTransactionNotFoundException
     */
    public function handle(): Response
    {
        $prizeFromRequest = $this->serializer->serialize( $this->request->getContent() );
        $this->validator->validate($prizeFromRequest);

        $rejectedTransaction = $this->getRejectedPrize( $prizeFromRequest->getId() );

        if ( ! $rejectedTransaction instanceof PhysicalPrizeTransaction ) {
            throw new RejectedPhysicalPrizeTransactionNotFoundException( Response::HTTP_NOT_FOUND );
        }

        //создаём транзакцию удаления
        $newTransactionReject = new PhysicalPrizeTransaction(
            PhysicalPrizeTransaction::STATUS_REJECT
            , $this->user
            , $rejectedTransaction->getPhysicalPrizeAvailable()
            , null
            , $rejectedTransaction
        );

        try {
            $this->objectManager->persist( $newTransactionReject );
            $this->objectManager->flush();
        } catch ( UniqueConstraintViolationException $e ) {
            return new Response( Response::HTTP_CONFLICT );

        }

        // восстанавливаем количество
        $this->prizeAvailableRepository->restorePrizeCountByOne( $rejectedTransaction->getPhysicalPrizeAvailable() );

        return new Response();
    }

    /**
     * Ищем транзакцию в котрой на числилои приз
     *
     * @param int $id
     *
     * @return PhysicalPrizeTransaction|null
     */
    private function getRejectedPrize( int $id ) : ? PhysicalPrizeTransaction
    {
        return $this->transactionRepository->findOneBy(['id' => $id]);

    }

    /**
     * Добавляем объект реквест в сервис
     *
     * @param Request $request
     *
     * @return mixed
     */
    public function setRequest( Request $request )
    {
        $this->request = $request;
    }

}