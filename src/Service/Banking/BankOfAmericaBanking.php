<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 21:24
 */

namespace App\Service\Banking;

use App\BankOfaAmericaBundle\Service\ApiClientInterface;
use App\Exception\UnSuccessPatchToBankException;
use App\GameEntity\BankForMoneyTransactions;
use App\Hdrator\Entity\MoneyTransactionForBanking;
use App\BankOfaAmericaBundle\SerialiseEntity\BankOfAmericaPostRequest;
use App\Service\MoneyTransactionCreatorInterface;
use Symfony\Component\Serializer\SerializerInterface;

class BankOfAmericaBanking extends AbstractBankService
{
    const BANK_ALIAS = "Bank of America";

    const CHUNK_SIZE = 1;

    /**
     * @var SerializerInterface
     */
    private $serializer;

    /**
     * @var MoneyTransactionCreatorInterface
     */
    protected $moneyTransactionCreator;

    /**
     * @var BankingServiceInterface
     */
    protected $nextBank;

    /**
     * @var ApiClientInterface
     */
    protected $apiClient;

    /**
     * BankOfAmericaBanking constructor.
     *
     * @param SerializerInterface              $serializer
     * @param MoneyTransactionCreatorInterface $transactionCreator
     * @param ApiClientInterface               $apiClient
     */
    public function __construct(
        SerializerInterface $serializer,
        MoneyTransactionCreatorInterface $transactionCreator,
        ApiClientInterface $apiClient
    ) {
        $this->serializer = $serializer;
        $this->moneyTransactionCreator = $transactionCreator;
        $this->apiClient = $apiClient;
    }

    /**
     * Проверяем можем ли сервис отправить деньги в банк пользователя
     *
     * @param BankForMoneyTransactions $bankForMoneyTransactions
     *
     * @return bool
     */
    function checkCapabilities( BankForMoneyTransactions $bankForMoneyTransactions ): bool
    {
        return $bankForMoneyTransactions->getBankAlias() === static::BANK_ALIAS;
    }

    /**
     * @param BankForMoneyTransactions $bankForMoneyTransactions
     *
     * @return mixed|string
     * @throws UnSuccessPatchToBankException
     */
    public function send( BankForMoneyTransactions $bankForMoneyTransactions )
    {
        if ( ! $this->checkCapabilities( $bankForMoneyTransactions ) ){
            return parent::send( $bankForMoneyTransactions );
        }

        $i = 0;
        $bachForSending = [];
        /** @var MoneyTransactionForBanking $moneyForBanking*/
        foreach ($bankForMoneyTransactions->getMoneyTransactionsForBanking() as $moneyForBanking) {

            $i += 1;

            $bankOfAmericaPutRequest = new BankOfAmericaPostRequest(
                $moneyForBanking->getUser()->getId()
                , $moneyForBanking->getAmount()->toFloat()
            );

            $bachForSending[] = $bankOfAmericaPutRequest;

            if ( $i === static::CHUNK_SIZE) {
                $json = $this->serializer->serialize($bachForSending, 'json');

                $requestSuccess = $this->apiClient->postMoney( $json );
                if ( $requestSuccess ) {
                    /**
                     * Если банк ответил OK создаём транзакции списания
                     */
                    $this->moneyTransactionCreator->createMoveToBankTransaction(
                        $moneyForBanking->getUser(),
                        $moneyForBanking->getAmount()
                    );
                } else {
                    throw new UnSuccessPatchToBankException();
                }
                $bachForSending = [];
                $i = 0;
            }
        }
    }


}