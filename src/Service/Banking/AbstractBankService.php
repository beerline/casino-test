<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 21:15
 */

namespace App\Service\Banking;


use App\GameEntity\BankForMoneyTransactions;

abstract class AbstractBankService implements BankingServiceInterface
{

    /**
     * @var BankingServiceInterface
     */
    protected $nextBank;


    /**
     * @param BankingServiceInterface $bankingService
     *
     * @return BankingServiceInterface
     */
    public function setNext( BankingServiceInterface $bankingService ) : BankingServiceInterface
    {
        $this->nextBank = $bankingService;

        return $bankingService;
    }

    /**
     * Отправляем данные в банк
     *
     * @param BankForMoneyTransactions $bankForMoneyTransactions
     *
     * @return mixed
     */
    public function send( BankForMoneyTransactions $bankForMoneyTransactions )
    {
        if ( $this->nextBank instanceof BankingServiceInterface) {
           return $this->nextBank->send( $bankForMoneyTransactions );
        }
    }

}