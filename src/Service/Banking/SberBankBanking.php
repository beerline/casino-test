<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 21:25
 */

namespace App\Service\Banking;

use App\GameEntity\BankForMoneyTransactions;

class SberBankBanking extends AbstractBankService
{
    const BANK_ALIAS = "Sberbank";

    /**
     * Проверяем можем ли сервис отправить деньги в банк пользователя
     *
     * @param BankForMoneyTransactions $bankForMoneyTransactions
     *
     * @return bool
     */
    function checkCapabilities( BankForMoneyTransactions $bankForMoneyTransactions ): bool
    {
        return $bankForMoneyTransactions->getBankAlias() === static::BANK_ALIAS;
    }

    /**
     * @param BankForMoneyTransactions $bankForMoneyTransactions
     *
     * @return mixed|string
     */
    public function send( BankForMoneyTransactions $bankForMoneyTransactions )
    {
        if ( ! $this->checkCapabilities( $bankForMoneyTransactions ) ){
            return parent::send( $bankForMoneyTransactions );
        }
        // TODO отправка в этот банк

        /**
         *
         * Реализация юудет зависить от конкретного банка-партнёра.
         * Поэтому привел пример сдела примерную реализацию в \App\Service\Banking\BankOfAmericaBanking::send
         *
         */

        return static::BANK_ALIAS;
    }
}