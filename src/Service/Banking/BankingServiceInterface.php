<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 21:16
 */

namespace App\Service\Banking;

use App\GameEntity\BankForMoneyTransactions;
use App\Hdrator\Entity\MoneyTransactionForBanking;

interface BankingServiceInterface
{
    /**
     * Устанавливаем следующее звено цепочки
     *
     * @param BankingServiceInterface $bankingService
     *
     * @return BankingServiceInterface
     */
    public function setNext( BankingServiceInterface $bankingService ) : BankingServiceInterface;

    /**
     * Отправляем данные в банк
     *
     * @param BankForMoneyTransactions $bankForMoneyTransactions
     *
     * @return mixed
     */
    public function send( BankForMoneyTransactions $bankForMoneyTransactions ) ;

    /**
     * Проверяем можем ли сервис отправить деньги в банк пользователя
     *
     * @param BankForMoneyTransactions $bankForMoneyTransactions
     *
     * @return bool
     */
    function checkCapabilities( BankForMoneyTransactions $bankForMoneyTransactions ) : bool;
}