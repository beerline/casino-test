<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 21:13
 */

namespace App\Service\Banking\ChainBuilder;

use App\Service\Banking\BankingServiceInterface;

interface BankingChainBuilderInterface
{
    /**
     * Формируем цепочку банковскх сервисов
     *
     * @return BankingServiceInterface
     */
    public function build() : BankingServiceInterface;
}