<?php
/**
 * Created by IntelliJ IDEA.
 * User: beerline
 * Date: 26.11.2018
 * Time: 21:40
 */

namespace App\Service\Banking\ChainBuilder;

use App\Service\Banking\BankingServiceInterface;
use App\Service\Banking\BankOfAmericaBanking;
use App\Service\Banking\SberBankBanking;
use App\Service\Banking\YetAnotherBank;

class BasicChainBuilder extends AbstractBankingChainBuilder
{
    /**
     * @var BankOfAmericaBanking
     */
    private $bankOfAmerica;

    /**
     * @var SberBankBanking
     */
    private $sberBank;

    /**
     * @var YetAnotherBank
     */
    private $yetAnotherBank;

    /**
     * BasicChainBuilder constructor.
     *
     * @param BankOfAmericaBanking $bankOfAmerica
     * @param SberBankBanking      $sberBank
     * @param YetAnotherBank       $anotherBank
     */
    public function __construct(
        BankOfAmericaBanking $bankOfAmerica,
        SberBankBanking $sberBank,
        YetAnotherBank $anotherBank
    ) {
        $this->bankOfAmerica = $bankOfAmerica;
        $this->sberBank = $sberBank;
        $this->yetAnotherBank = $anotherBank;
    }

    /**
     * Формируем цепочку банковскх сервисов
     *
     * @return BankingServiceInterface
     */
    public function build(): BankingServiceInterface
    {
        $this->yetAnotherBank->setNext($this->bankOfAmerica)->setNext($this->sberBank);

        return $this->yetAnotherBank;
    }
}